#include "BorderBuilder.h"
#include "OgreResourceGroupManager.h"
#include "OgrePlane.h"

#include "Border.h"

BorderBuilder::BorderBuilder()
{
    //ctor
}

BorderBuilder::~BorderBuilder()
{
    //dtor
}

void BorderBuilder::BuildPart(Ogre::SceneManager* scene_manager, Ogre::MeshManager* mesh_mgr, int degree, int height, int width, int wallHeight, Ogre::Vector3 vec, Entity* ent){
    ((Border*) ent)->degree = degree;
    Ogre::Vector3 pos = ent->GetPosition();
    pos.y += wallHeight/2;
    ent->SetPosition(pos);
    //std::cout << "wallHeight: "<< wallHeight << std::endl;

    std::string entity_name = "BorderEntity" + std::to_string(ent->GetId());
    std::string resource_name = "Resource" + std::to_string(ent->GetId());
    ((Border*) ent)->resource_name = resource_name;
    std::string resource_group = Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME;
    //std::cout << "attempting to create plane\n";

    int ycomponent = vec.y;

    if ( ycomponent == 1){
        Ogre::Plane plane (vec, wallHeight/2);
        //std::cout << resource_group << std::endl;
        //std::cout << entity_name << std::endl;
        mesh_mgr->createPlane(resource_name, resource_group, plane, width, height, 20, 20, true, 1, 5, 5, Ogre::Vector3::UNIT_Z);
        //std::cout << "plane created" << std::endl;
    }
    else {
        Ogre::Plane plane (vec, 0);
        //std::cout << resource_group << std::endl;
        //std::cout << entity_name << std::endl;
        mesh_mgr->createPlane(resource_name, resource_group, plane, wallHeight, width, 20, 20, true, 1, 5, 5, Ogre::Vector3::UNIT_Z);
        //std::cout << "plane created" << std::endl;

    }
    ent->Init(scene_manager);
}
