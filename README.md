Source code for a school project using ogre3D, showing a 3D scene with some basic game functionalities, requires cmake to build and run. 
The application allows placement of towers in a small 3D environment, enemies will find a path around those using an A-Star search algorithm (see Pathplanning.cpp).


To start working with the code, ogre1.9 and it's dependencies and cmake need to be installed.
I've used cmake-gui to generate project files for Codeblocks under Linux, but other IDEs and OS supported by ogre should work as well.


The main state of the game after initialization is GameState.cpp so it's a good place to start when adding new functionality.
New .cpp files and .h files need to be registered in CMakeLists.txt under set(SRCS ... ) or they will be ignored by cmake.




