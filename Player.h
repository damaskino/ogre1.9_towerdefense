#ifndef PLAYER_H
#define PLAYER_H

#include <OgreVector3.h>
#include <OISKeyboard.h>
#include <OISMouse.h>
#include <SdkTrays.h>
#include <OgreTimer.h>
#include "Level.h"

enum ESelectedTower
{
	None,
	BasicTower,
	SlowingTower,
};

class EntityManager;
class EntityFactory;
class Player
{

    private:
        void Pick(float x, float y);

        Ogre::Vector3 m_raycast_hit;
        ESelectedTower m_selected_tower;
        const unsigned int BasicTowerPrice;
        const unsigned int SlowingTowerPrice;
        EntityManager* m_entity_manager;
        EntityFactory* m_entity_factory;
        bool						m_bLMouseDown, m_bRMouseDown;
        Ogre::RaySceneQuery*		m_pRSQ;
        Ogre::Camera*				m_pCamera;
        OIS::Keyboard*				m_pKeyboard;
        OIS::Mouse*					m_pMouse;
        bool                        m_bTowerSelected;
        Ogre::Entity*               m_pseudoEntity;
        Ogre::SceneNode*            m_pseudoSceneNode;
        Ogre::SceneManager*         m_pSceneMgr;
        int                         m_pIncome;
        Level*                      m_pLevel;

        void buyTower(ESelectedTower entity_type, Ogre::Vector3 pos);

    public:
        static int                  m_pLife;
        static int                  m_pGold;
        static int                  m_killCount;
        //EnemyLevelMediator*          enemy_level_mediator;

        /** Default constructor */
        Player(EntityManager* entity_manager, EntityFactory* entity_factory, Ogre::Camera* m_pCamera, Ogre::RaySceneQuery* m_pRSQ, OIS::Keyboard* m_pKeyboard, OIS::Mouse* m_pMouse, Ogre::SceneManager* m_pSceneMgr, Level* m_pLevel);

        /** Default destructor */
        virtual ~Player();

        void IncreaseIncome();
        void AddIncome();
        static void DecreaseLive(){ m_pLife--; }
        static void IncreaseKillCount(){m_killCount++; }
        int GetGold();
        int GetIncome();
        void AddLive(unsigned int value);
        int GetLife();
        int GetKillCount();
        bool PurchaseTower();
        void Update(Ogre::Vector3 hit);
        void getInput(bool &m_bSettingsMode);
        bool keyPressed(const OIS::KeyEvent &keyEventRef);
        bool keyReleased(const OIS::KeyEvent &keyEventRef);
        bool buttonHit(const Ogre::String& name);

        void onLeftPressed(const OIS::MouseEvent &evt);

        bool mouseMoved(const OIS::MouseEvent &evt);
        bool mousePressed(const OIS::MouseEvent &evt, OIS::MouseButtonID id);
        bool mouseReleased(const OIS::MouseEvent &evt, OIS::MouseButtonID id);


    protected:

};

#endif // PLAYER_H
