#ifndef BASICTOWER
#define BASICTOWER

#include "Entity.h"

class EntityManager;
class EntityFactory;
class BasicTower : public Entity
{
public:
	BasicTower(const EEntityType& entity_type, EntityFactory* entity_factory, EntityManager* entity_manager);
	virtual ~BasicTower(void);

	void Init(Ogre::SceneManager* scene_manager);
	void Update(double deltatime);
    void SetPosition(Ogre::Vector3 pos);
	void SetShootTime(float shoot_time);
	//void SetNearestEnemy(int nNEnemy);
	void AimAtEnemy(Entity* enemy, double timeSinceLastFrame);
	void Shoot(Ogre::Vector3 enemyPos, double timeSinceLastFrame);
	void SetAnimationState(char* animationstate);
    float m_radius;
    bool HasBullet();
    void SetBullet(bool boolean);
    int GetDamage();
    void ResetFireRate();
    //Entity* nearestEnemies[];
    std::vector<Entity*> nearestEnemies;


protected:
	EntityFactory*  m_entity_factory;
	EntityManager*  m_entity_manager;
private:
    bool    m_isShooting;
    bool    m_Bullet;
    char*   m_animationState;
    int     m_nearestEnemy;
    Entity* m_bulletEnt;
    Entity* m_enemy;
    Ogre::Vector3 m_despawnAim;
    int     m_damage;
    float   m_fireRate;
};

#endif //BASICTOWER
