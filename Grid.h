#ifndef GRID_H
#define GRID_H

#include <Ogre.h>

class Grid
{
public:
    Grid()
    {
    };

    virtual ~Grid()
	{
	};

	    //The grid's ManualObject
    Ogre::ManualObject* mGrid;

    /*
        mOwnNode - under this node is the manual object.
        The Cell size is achieved by scaling this node.
        The Offset is achieved by translating this node.
        use attachToNode() function to attach the grid to your own node.
        mAttachedNode is the node that the grid is attached.
        mOwnNode is created by this grid, you should not destroy it.
        mAttachedNode is node from your own application.
        To transform and rotate the grid, you should manipulate your own node.
        The Grid's Zero offset and Cell Size are grid features despite the fact that their imlementation
        is done by just translation and scaling mOwnNode.
        You may need to access the mOwnNode itself to get its transfrmation if you need to, but altering it
        will be lost on next call to update().
        An example snapping function is provided in the Ogre's wiki and forum post
     */
    Ogre::SceneNode *mOwnNode, *mAttachedNode;

    //The material to use for the ManualObject.
    //Note that BaseWhiteNoLighting does not support alpha blending
    Ogre::String material;

    //These are the grid settings.
    // After you complete them, call update() to regenerate the grid.
    // These are public data members as intended, don't blame me for not using setters/getters

    //Cell sizes
    float csX, csY, csZ;

    //Grid's (0,0,0) offset
    float ofX, ofY, ofZ;

    //Number of divisions in each dimension
    int divX, divY, divZ;

    //Will the divisions be shown?
    bool showDivisions;

    //The grid is limited in each dimension, and these are its min and max limiters
    int xmin, xmax,
        ymin, ymax,
        zmin, zmax;

    // Which planes to show? You can show only planes you need
    bool showXY, showXZ, showYZ;

    //offset color is the color of (0,0,0) axis
    //master color is the usual master color
    //division color is the color of the subdivisions
    Ogre::ColourValue colorMaster, colorOffset, colorDivision;

    //The Ogre RenderQueue where to draw the grid - RENDER_QUEUE_MAIN/OVERLAY/BACKGROUND/ any int
    int drawQueue;

    //Attach the grid to your provided sceneNode.
    virtual void attachToNode(Ogre::SceneNode* node) = 0;

    //After setup, call this to regenerate the Grid.
    virtual void update() = 0;

    //Grid's visibility
    virtual void hide() = 0;
    virtual void show() = 0;
};

#endif // GRID_H
