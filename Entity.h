#ifndef ENTITY_H
#define ENTITY_H

#include <OgreVector3.h>
#include <OgreEntity.h>
#include <OgreSceneManager.h>
#include <OgreSceneNode.h>

class Entity;
class EntityListener
{
    public:
        EntityListener(void){}
        virtual ~EntityListener(void){}
        virtual void RemoveEntity(Entity* entity) = 0;

    protected:
};

enum EEntityType
{
    Basic_Tower,
    Enemy_Type,
    Particle_Type,
    Border_Type,
    Bullet_Type
};

class Entity
{
public:
    Entity(const EEntityType& entity_type) : m_listener(NULL)
    {
        static unsigned int id_gen = 0;
        m_id = id_gen++;
        m_type = entity_type;
        m_position = Ogre::Vector3(0.0f, 0.0f, 0.0f);
		m_rotation = Ogre::Vector3(0.0f, 0.0f, 0.0f);
    };

    virtual ~Entity()
	{
	}

    virtual void Init(Ogre::SceneManager* scene_manager) = 0;
    virtual void Update(double deltatime) = 0;

    virtual void SetPosition(Ogre::Vector3 pos) = 0;
    Ogre::Vector3 GetPosition() { return m_position; }
    void SetEntityListener(EntityListener* listener) {m_listener = listener;}
    Ogre::SceneNode* GetSceneObject() const { return m_scene_object; }
	void RemoveMe() { m_listener->RemoveEntity(this); }
	unsigned int GetId() { return m_id; }
	EEntityType GetType() { return m_type; }
	void SetType(EEntityType Type){m_type = Type;}
	void SetMaterialByName(char* material) {m_material = material;}
	char* GetMaterialName() {return m_material;}
	Ogre::Entity* GetOgreEntity() {return m_OgreEntity;}

protected:
    unsigned int m_id;
    Ogre::SceneNode* m_scene_object;
	Ogre::Vector3 m_position;
	Ogre::Vector3 m_rotation;
	EEntityType m_type;
	EntityListener* m_listener;
	char* m_material;

private:
    Ogre::Entity* m_OgreEntity;
};
#endif // ENTITY_H
