#include "EntityFactory.h"
#include "EntityManager.h"
#include "BasicTower.h"
#include "Particle.h"
#include "Enemy.h"
#include "Border.h"
#include "Bullet.h"

EntityFactory::EntityFactory(Ogre::SceneManager* scene_manager, MyParticleSystem* m_PS)
:m_entity_manager(nullptr),  m_scene_manager(scene_manager), m_particle_system(m_PS)
{

}

EntityFactory::~EntityFactory(void){
}


Entity* EntityFactory::Create(EEntityType entity_type, Ogre::Vector3 pos)
{
	::Entity *entity;
	::Entity **pp = &entity;

	switch (entity_type)
	{
    case EEntityType::Basic_Tower:
		{
			entity = new BasicTower(entity_type, this, m_entity_manager);
			entity->SetMaterialByName("robot.mesh");
            entity->Init(m_scene_manager);
			entity->SetPosition(pos);
            entity->SetEntityListener((EntityListener*) m_entity_manager);
			break;
		}
    case EEntityType::Enemy_Type:
        {
            entity = new Enemy(entity_type, this, m_entity_manager);
            entity->SetMaterialByName("ninja.mesh");
            entity->Init(m_scene_manager);
            entity->SetPosition(pos);
            entity->SetEntityListener((EntityListener*) m_entity_manager);
            break;
        }
	case EEntityType::Particle_Type:
		{
			entity = new Particle(entity_type, this, m_entity_manager);
			//entity->SetMaterialByName("");
			((Particle*) entity)->AddEmitter(m_particle_system);
            entity->SetPosition(pos);
            entity->Init(m_scene_manager);
			entity->SetEntityListener((EntityListener*) m_entity_manager);
			break;
		}

    case EEntityType::Border_Type:
        {
            entity = new Border(entity_type, this, m_entity_manager);
            entity->SetPosition(pos);
            entity->SetEntityListener((EntityListener*) m_entity_manager);
            break;
        }
    case EEntityType::Bullet_Type:
        {
            entity = new Bullet(entity_type, this, m_entity_manager);
            entity->SetMaterialByName("Examples/Marmor");
            entity->Init(m_scene_manager);
            entity->SetPosition(pos);
            entity->SetEntityListener((EntityListener*) m_entity_manager);
            break;
        }
	};

	return entity;
}
