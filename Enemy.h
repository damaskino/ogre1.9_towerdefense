#ifndef ENEMY
#define ENEMY

#include "Entity.h"
#include "OgreSceneManager.h"
#include "OgreEntity.h"
#include "EnemyLevelMediator.h"

class EntityManager;
class EntityFactory;
class Enemy : public Entity
{
public:
	Enemy(const EEntityType& entity_type, EntityFactory* entity_factory, EntityManager* entity_manager);
	virtual ~Enemy(void);

	void Init(Ogre::SceneManager* scene_manager);
	void Update(double deltatime);

    void FollowPath(double deltatime);
    void PlayAnimation(Ogre::Entity* OgrEnt, double deltatime);

    void AppendToPath(std::vector<Ogre::Vector3> *path);
    void SetPosition(Ogre::Vector3 pos);
	void SetLife(int value);
	void SetMaxHealthPoints(int newHealthPoints);
    void SetPath(std::vector<Ogre::Vector3> *path);
  	void SetHealthBar(Ogre::SceneManager* scene_manager);
  	void TurnBack();
  	void PrintPath();
    int GetCurrentPathIndex();
    Ogre::Vector3 GetLastPosition();

	int GetLife();
	Ogre::Real GetSpeed();
	Ogre::Billboard* GetBillBoard();
	float GetMaxHealthPoints();

    void ChangeRotation(Ogre::Vector3);

protected:
	EntityFactory* m_entity_factory;
	EntityManager* m_entity_manager;
	EnemyLevelMediator* m_enemy_level_mediator;
private:
    Ogre::Real speed;
    int m_hitPoints;
    int m_maxHitPoints;

    int m_currentPathIndex;
    Ogre::Vector3 m_currentPathPos;
    Ogre::Billboard* m_bBoard;
    Ogre::Vector3 m_lastPosition;
    std::vector<Ogre::Vector3> m_path;
};

#endif //ENEMY_H

