#ifndef PATHPLANNING_H
#define PATHPLANNING_H
#include <queue>
#include <functional>
#include <set>
#include "OgreVector3.h"


struct node {
    node* parent;
    int col, row;
    int id;
    float f, g, h;
    int parentCol;
    int parentRow;
};


const int colMaximum = 32;
const int rowMaximum = 32;
//Data structures required for Pathplanning



class cmp{
 public:
       bool operator() (const node left, const node right) const
       {
         return left.f > right.f;
       }
};

typedef std::priority_queue<float, std::vector<node>, cmp> NodeQueue;



inline bool operator<(const node &a, const node &b){
        return a.id < b.id;
}



class Pathplanning
{
    public:

        Pathplanning();
        virtual ~Pathplanning();
        void update();
        void init();
        std::vector<Ogre::Vector3>  tracePath(node,node);
        void findPath(int, int);
        float distance(node,node);
        void printNodeGrid();
        void printGrid();
        void clearQueue();
        std::vector<node> checkNeighbours(node*);
        float convertToWorldCoord(int);

        bool findInQueue(NodeQueue, node);
        NodeQueue removeFromQueue(NodeQueue, node);

        node m_NodeArray[colMaximum][rowMaximum];

        float m_GridArray[32][32];
        std::vector<Ogre::Vector3> m_calculatedPath;

        std::vector<Ogre::Vector3> getPath();


    protected:
    private:
        int m_idCount = 0;

        std::set<node> m_closed;
        NodeQueue m_open;

        //priority queue goes here
};

#endif // PATHPLANNING_H
