#include "Player.h"
#include "EntityManager.h"
#include "EntityFactory.h"
#include <OgreMath.h>
#include "Enemy.h"
#include "AdvancedOgreFramework.h"
#include <typeinfo>

Player::Player(EntityManager* entity_manager, EntityFactory* entity_factory, Ogre::Camera* m_pCamera, Ogre::RaySceneQuery* m_pRSQ, OIS::Keyboard* m_pKeyboard, OIS::Mouse* m_pMouse, Ogre::SceneManager* m_pSceneMgr, Level* m_pLevel) :
    m_entity_factory(entity_factory), m_entity_manager(entity_manager), m_pCamera(m_pCamera), m_pRSQ(m_pRSQ), m_pKeyboard(m_pKeyboard), m_pMouse(m_pMouse), BasicTowerPrice(20), SlowingTowerPrice(40), m_pSceneMgr(m_pSceneMgr), m_pLevel(m_pLevel)
{
    //ctor
    m_bLMouseDown       = false;
    m_bRMouseDown       = false;
    //m_pLife             = 100;
    m_pIncome           = 2;
    m_bTowerSelected    = false;

}

Player::~Player()
{
    //dtor
}

bool Player::keyPressed(const OIS::KeyEvent &keyEventRef)
{
    return true;
}



bool Player::keyReleased(const OIS::KeyEvent &keyEventRef)
{
    return true;
}



bool Player::mouseMoved(const OIS::MouseEvent &evt)
{
    if(m_bRMouseDown)
    {
        m_pCamera->yaw(Ogre::Degree(evt.state.X.rel * -0.1f));
        m_pCamera->pitch(Ogre::Degree(evt.state.Y.rel * -0.1f));
    }
    Ogre::Ray mouseRay = m_pCamera->getCameraToViewportRay(m_pMouse->getMouseState().X.abs / float(evt.state.width),
        m_pMouse->getMouseState().Y.abs / float(evt.state.height));
    m_pRSQ->setRay(mouseRay);
    m_pRSQ->setSortByDistance(true);

    Ogre::RaySceneQueryResult &result = m_pRSQ->execute();
    Ogre::RaySceneQueryResult::iterator itr;

    for(itr = result.begin(); itr != result.end(); itr++)
    {
        if(itr->movable)
        {
            int flags = itr->movable->getQueryFlags();
            //OgreFramework::getSingletonPtr()->m_pLog->logMessage("Flags: "+std::to_string(flags));
            //Ogre::String Name = itr->movable->getName();
            //if(Name == "GroundEntity")
            if (flags==2)
            {
                break;
            }
            if (flags==1 && m_bTowerSelected)
            {
                Ogre::Vector3 pos = mouseRay.getPoint(itr->distance);
                Ogre::Vector3 r = pos;
                Ogre::Vector3 divsLevel = m_pLevel->getDivisions();

                //OgreFramework::getSingletonPtr()->m_pLog->logMessage("Divisions " + Ogre::StringConverter::toString(divsLevel));

                //should we snap to divisions or just to master lines?
                bool snapToDivs = true;
                Ogre::Vector3 divs = snapToDivs ? divsLevel : Ogre::Vector3(1,1,1);

                //undo the scale, translation and rotation of the Grid Plane and multiply to division count
                r = m_pLevel->mOwnNode->_getFullTransform().inverse() * r;
                //OgreFramework::getSingletonPtr()->m_pLog->logMessage("r inverse transform " + Ogre::StringConverter::toString(r));
                r *= divs;

                //OgreFramework::getSingletonPtr()->m_pLog->logMessage("r before snapping " + Ogre::StringConverter::toString(r));

                //snap the point! Just a round!
                r.x = round(r.x);
                r.y = round(r.y);
                r.z = round(r.z);

                //OgreFramework::getSingletonPtr()->m_pLog->logMessage("r rounded " + Ogre::StringConverter::toString(r));

                //divide the divisions, redo the scale, translation and rotation of the Grid Plane
                r /= divs;
                r = m_pLevel->mOwnNode->_getFullTransform() * r;
                //OgreFramework::getSingletonPtr()->m_pLog->logMessage("r snapped" + Ogre::StringConverter::toString(r));

                //don't go out of bounds
                r.x = std::min(1000.0f , std::max(-1000.0f, r.x));
                r.z = std::min(1000.0f , std::max(-1000.0f, r.z));

                std::tuple<int, int> t = m_pLevel->posInGridCell(r.x, r.z, 62.5);
                //OgreFramework::getSingletonPtr()->m_pLog->logMessage(m_pLevel->posInGrid(r.x, r.z, 62.5));
                int col = std::get<0>(t);
                //OgreFramework::getSingletonPtr()->m_pLog->logMessage(std::to_string(col));
                int row = std::get<1>(t);
                //OgreFramework::getSingletonPtr()->m_pLog->logMessage(std::to_string(row));
                if (col > 0){
                    r.z = col * 62.5 - 31.25;
                }
                else{
                    r.z = col * 62.5 + 31.25;
                }
                if (row > 0){
                    r.x = row * 62.5 - 31.25;
                }
                else{
                    r.x = row * 62.5 + 31.25;
                }

                // set position for pseudo/preview node
                pos = r;
                m_pseudoSceneNode->setPosition(pos);
                std::tuple<int, int> t1 = m_pLevel->posInGrid(r.x, r.z, 62.5);
                //OgreFramework::getSingletonPtr()->m_pLog->logMessage(m_pLevel->posInGrid(r.x, r.z, 62.5));
                int col1 = std::get<0>(t1);
                //OgreFramework::getSingletonPtr()->m_pLog->logMessage(std::to_string(col1));
                int row1 = std::get<1>(t1);
                //OgreFramework::getSingletonPtr()->m_pLog->logMessage(std::to_string(row1));
            }
        }
    }
    return true;
}


bool Player::mousePressed(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
    if(id == OIS::MB_Left)
    {
        onLeftPressed(evt);
        m_bLMouseDown = true;
    }
    else if(id == OIS::MB_Right)
    {
        m_bRMouseDown = true;
    }

    return true;
}

bool Player::mouseReleased(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
    if(id == OIS::MB_Left)
    {
        m_bLMouseDown = false;
    }
    else if(id == OIS::MB_Right)
    {
        m_bRMouseDown = false;
    }
    return true;
}

void Player::onLeftPressed(const OIS::MouseEvent &evt)
{
    Ogre::Ray mouseRay = m_pCamera->getCameraToViewportRay(m_pMouse->getMouseState().X.abs / float(evt.state.width),
        m_pMouse->getMouseState().Y.abs / float(evt.state.height));
    m_pRSQ->setRay(mouseRay);
    m_pRSQ->setSortByDistance(true);
    //m_pRSQ->setQueryMask(4);

    Ogre::RaySceneQueryResult &result = m_pRSQ->execute();
    Ogre::RaySceneQueryResult::iterator itr;

    for(itr = result.begin(); itr != result.end(); itr++)
    {
        if(itr->movable)
        {
            int flags = itr->movable->getQueryFlags();
            OgreFramework::getSingletonPtr()->m_pLog->logMessage("Flags: "+std::to_string(flags));
            OgreFramework::getSingletonPtr()->m_pLog->logMessage("Name: "+itr->movable->getName());
            //if(Name == "GroundEntity")
            if (flags==2)
            {
                break;
            }
            if (flags==1 && m_bTowerSelected)
            {
                //Ogre::Vector3 pos = mouseRay.getPoint(itr->distance);
                Ogre::Vector3 pos = m_pseudoSceneNode->getPosition();
                //createEntity
                this->buyTower(m_selected_tower, pos);
                m_pseudoSceneNode->detachAllObjects();
                m_pSceneMgr->destroyEntity(m_pseudoEntity);
                m_pseudoEntity = NULL;
                m_pSceneMgr->getRootSceneNode()->removeAndDestroyChild("PseudoNode");
                m_pseudoSceneNode = NULL;
                m_bTowerSelected = false;
                m_selected_tower = None;
            }
        }
    }
}

void Player::buyTower(ESelectedTower entity_type, Ogre::Vector3 pos){
    switch (entity_type)
    {
    case ESelectedTower::BasicTower:
        //std::cout << "Tower bought" << std::endl;
        {

            std::tuple<int,int> towerPos = m_pLevel->posInGrid(pos[0],pos[2], 62.5);
            int col = std::get<0>(towerPos);
            int row = std::get<1>(towerPos);


            if (m_pGold >= 15 && !m_pLevel->goalBlocked(col, row)){
                m_entity_factory->Create(Basic_Tower, pos);
                //m_entity_factory->Create(Particle_Type, pos);
                //Get the cell where the tower was placed, then block that cell in the grid array
                std::cout << "Tower was placed at:" << std::endl;
                std::cout << "Column" << col << std::endl;
                std::cout << "Row" << row << std::endl;
                this->m_pGold -= 15;

                //TODO
                m_pLevel->m_GridArray[col][row]= INFINITY;
                m_pLevel->updateMediator();
                m_pLevel->printGrid();

            } else {
                //TODO display a message
                 std::cout << "Path blocked" << std::endl;
                //m_pLevel->printGrid();

            }
            break;
        }
    };
}

void Player::getInput(bool &m_bSettingsMode)
{
    /*if(m_bSettingsMode == false)
    {
        if(OgreFramework::getSingletonPtr()->m_pKeyboard->isKeyDown(OIS::KC_A))
            m_bSettingsMode = !m_bSettingsMode;
    }*/
}

void Player::IncreaseIncome(){
    this->m_pIncome += 2;
}

void Player::AddIncome(){
    this->m_pGold += m_pIncome;
}

int Player::GetIncome(){
    return m_pIncome;
}

int Player::GetGold(){
    return m_pGold;
}

int Player::GetKillCount(){
    return m_killCount;
}


bool Player::buttonHit(const Ogre::String& name){
    if (name == "Basic_Tower"){
        if (!m_pSceneMgr->hasEntity("pseudo")){
            m_pseudoEntity = m_pSceneMgr->createEntity("pseudo", "robot.mesh");
            m_pseudoSceneNode = m_pSceneMgr->getRootSceneNode()->createChildSceneNode("PseudoNode");
            m_pseudoSceneNode->yaw(Ogre::Degree(180));
            m_pseudoSceneNode->attachObject(m_pseudoEntity);
            m_bTowerSelected = true;
            m_selected_tower = BasicTower;
        }
        else{

            return false;
        }
    }

}

