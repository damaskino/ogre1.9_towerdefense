#include "Enemy.h"
#include "EntityFactory.h"
#include "EntityManager.h"
#include <OgreAnimationState.h>
#include "Player.h"
#include <math.h>
//#include "EnemyLevelMediator.h"

Enemy::Enemy(const EEntityType& entity_type, EntityFactory* entity_factory, EntityManager* entity_manager) : Entity(entity_type){
	m_entity_factory = entity_factory;
	m_entity_manager = entity_manager;
	//default aim
	speed = 0.1f;
	m_hitPoints = 100;
	m_maxHitPoints = 100;
	m_lastPosition = NULL;
	m_bBoard = NULL;

    //The current coordinate on the path
	m_currentPathIndex = 0;
}

Enemy::~Enemy(void){

}

void Enemy::Init(Ogre::SceneManager* scene_manager){
    Ogre::Entity* OgreEnt = scene_manager->createEntity(std::to_string(this->GetId()), this->GetMaterialName());
    m_scene_object = scene_manager->getRootSceneNode()->createChildSceneNode(std::to_string(this->GetId()));

    SetHealthBar(scene_manager);

    m_scene_object->attachObject(OgreEnt);
    m_scene_object->scale(0.30,0.30,0.30);
    m_scene_object->showBoundingBox(true);
	m_entity_manager->AddEntity(this);
}

void Enemy::Update(double deltatime){

    Ogre::Entity* OgreEnt = (Ogre::Entity*)this->GetSceneObject()->getAttachedObject(Ogre::StringConverter::toString(this->GetId()));
    PlayAnimation(OgreEnt, deltatime);
    FollowPath(deltatime);


    //REFACTORING
    if (this->GetLife() <= 0){
        m_entity_manager->RemoveEntity(this);
        Player::IncreaseKillCount();
    }

    //Check life during every update, if life reaches zero, increment killcount

}

//Note: m_path has been set by this point, in Init() the path is still empty!
void Enemy::FollowPath(double deltatime){

    if (!m_path.empty()) {
        m_currentPathPos = m_path.back();


    } else {
        std::cout << "Removing Enemy" << std::endl;
        m_entity_manager->RemoveEntity(this);
        Player::DecreaseLive();
    }

    //When we've reached the current node in the path, change the goal to the next node
    Ogre::Vector3 currentEnemyPos = this->GetPosition();

    int x_distance = m_currentPathPos.x - currentEnemyPos.x;
    int z_distance = m_currentPathPos.z - currentEnemyPos.z;
    if (x_distance == 0 and z_distance == 0){
        m_lastPosition = m_path.back();
        m_path.pop_back();
        ChangeRotation(m_path.back());
        m_currentPathIndex++;

    }

    //Move the enemy towards the current coordinate in the path
    Ogre::Vector3 targetLocation = m_currentPathPos;
    Ogre::Vector3 direction = targetLocation - this->GetPosition();
    direction.normalise();
    Ogre::Real movement = speed * deltatime;
    this->GetSceneObject()->translate(movement * direction);
    Ogre::Vector3 pos = this->GetSceneObject()->getPosition();
    this->m_position = pos;
}


void Enemy::TurnBack(){

    std::cout << "Turning back!" << std::endl;
    std::cout << "Last position: " << m_lastPosition.x << std::endl;
    std::cout << "Targeted position: " << m_currentPathPos.x << std::endl;
    m_scene_object->lookAt(m_lastPosition, Ogre::Node::TS_PARENT, Ogre::Vector3::NEGATIVE_UNIT_Z);
    std::vector<Ogre::Vector3> backPath;
    backPath.push_back(m_lastPosition);

    std::cout << "Setting path to " << "X: " << m_lastPosition.x << "Z: " << m_lastPosition.z << std::endl;



    SetPath(&backPath);

    PrintPath();

}

void Enemy::AppendToPath(std::vector<Ogre::Vector3>* path){

    m_path.insert(m_path.end(), path->begin(), path->end());

}




void Enemy::SetHealthBar(Ogre::SceneManager* scene_manager){

    Ogre::BillboardSet* bBSet = scene_manager->createBillboardSet(std::to_string(this->GetId())+"BBSet");
    bBSet->setAutoextend(true);
    bBSet->setAutoUpdate(true);
    bBSet->setMaterialName("BaseWhiteAlphaBlended");

    Ogre::Billboard* bBoard = bBSet->createBillboard(Ogre::Vector3(0, 200, 0), Ogre::ColourValue::Green);
    m_bBoard = bBoard;
    bBoard->setDimensions(m_maxHitPoints, 12);
    Ogre::SceneNode* bBNode = m_scene_object->createChildSceneNode(std::to_string(this->GetId())+"bB");
    bBNode->attachObject(bBSet);
}


void Enemy::PlayAnimation(Ogre::Entity* OgreEnt, double deltatime){
    Ogre::AnimationState* animationState = OgreEnt->getAnimationState("Walk");
    animationState->setEnabled(true);
    animationState->addTime(deltatime / 1000.0f);
    animationState->setLoop(true);
}

//Rotate enemy to the given position.
//Uses lookAt() for now, but consider switching to quaternion rotation if this gives any problems.
void Enemy::ChangeRotation(Ogre::Vector3 destination){
    m_scene_object->lookAt(destination, Ogre::Node::TS_PARENT, Ogre::Vector3::NEGATIVE_UNIT_Z);
}


void Enemy::SetPosition(Ogre::Vector3 pos){
    m_position = pos;
    m_scene_object->setPosition(pos);
}


void Enemy::SetLife(int life){
    m_hitPoints = life;
}


int Enemy::GetLife(){
    return m_hitPoints;
}


Ogre::Real Enemy::GetSpeed(){
    return this->speed;
}


Ogre::Billboard* Enemy::GetBillBoard(){
    return this->m_bBoard;
}


float Enemy::GetMaxHealthPoints(){
    return (float)this->m_maxHitPoints;
}


int Enemy::GetCurrentPathIndex(){

    return m_currentPathIndex;
}


Ogre::Vector3 Enemy::GetLastPosition(){

    return m_lastPosition;
}


void Enemy::SetMaxHealthPoints(int newHealthPoints){
    this->m_maxHitPoints = newHealthPoints;
    m_bBoard->setDimensions(100.0, 12.0);
}

//For Debugging
void Enemy::PrintPath(){
        std::cout << "---------Current Path of enemy------------- " << std::endl;
        for(auto it = m_path.begin(); it != m_path.end(); it++){
            std::cout << "X: "<< (*it).x << " Z: " << (*it).z << std::endl;
        }
 }


void Enemy::SetPath(std::vector<Ogre::Vector3>* path){
    m_path = *path;
}
