#include "Bullet.h"
#include <OgreManualObject.h>
#include <OgreRenderOperation.h>
#include <OgreMesh.h>
#include <OgreMath.h>
#include "EntityFactory.h"
#include "EntityManager.h"
#include "Enemy.h"
#include "BasicTower.h"
#include <OgreBillboard.h>

Bullet::Bullet(const EEntityType& entity_type, EntityFactory* entity_factory, EntityManager* entity_manager) : Entity(entity_type)
{
    m_entity_factory = entity_factory;
	m_entity_manager = entity_manager;
	speed = 0.3f;
	m_timeToImpact = 0.0;
	m_distance = 0.0;
	m_enemyToHit = 0;
    m_tower = 0;
}

Bullet::~Bullet()
{
    //dtor
}

void Bullet::Init(Ogre::SceneManager* scene_manager){
    std::string strName = "SphereMesh"+std::to_string(this->GetId());
    //std::cout << "Sphere " << strName <<  " created" <<  std::endl;
    const float r = 10;
    const int nRings = 32;
    const int nSegments = 32;

    Ogre::ManualObject * manual = scene_manager->createManualObject(strName);
    manual->begin("BaseWhiteNoLighting", Ogre::RenderOperation::OT_TRIANGLE_LIST);

    float fDeltaRingAngle = (Ogre::Math::PI / nRings);
    float fDeltaSegAngle = (2 * Ogre::Math::PI / nSegments);
    unsigned short wVerticeIndex = 0 ;

    // Generate the group of rings for the sphere
    for( int ring = 0; ring <= nRings; ring++ ) {
        float r0 = r * sinf (ring * fDeltaRingAngle);
        float y0 = r * cosf (ring * fDeltaRingAngle);

        // Generate the group of segments for the current ring
        for(int seg = 0; seg <= nSegments; seg++) {
            float x0 = r0 * sinf(seg * fDeltaSegAngle);
            float z0 = r0 * cosf(seg * fDeltaSegAngle);

            // Add one vertex to the strip which makes up the sphere
            manual->position( x0, y0, z0);
            manual->normal(Ogre::Vector3(x0, y0, z0).normalisedCopy());
            manual->textureCoord((float) seg / (float) nSegments, (float) ring / (float) nRings);

            if (ring != nRings) {
                // each vertex (except the last) has six indicies pointing to it
                manual->index(wVerticeIndex + nSegments + 1);
                manual->index(wVerticeIndex);
                manual->index(wVerticeIndex + nSegments);
                manual->index(wVerticeIndex + nSegments + 1);
                manual->index(wVerticeIndex + 1);
                manual->index(wVerticeIndex);
                wVerticeIndex ++;
            }
        } // end for seg
    } // end for ring
    manual->end();
    Ogre::MeshPtr mesh = manual->convertToMesh(strName);

    //std::cout << "resourcenameSphere " << "Resource " + std::to_string(this->GetId()) << std::endl;
    //std::cout << "ID " << std::to_string(this->GetId()) << std::endl;
    Ogre::Entity* sphereEntity = scene_manager->createEntity("SphereEntity"+std::to_string(this->GetId()), strName);
    m_scene_object = scene_manager->getRootSceneNode()->createChildSceneNode(std::to_string(this->GetId()));
    sphereEntity->setMaterialName(m_material);
    m_scene_object->attachObject(sphereEntity);
    m_entity_manager->AddEntity(this);

    //For collision detection using AABB and Sphere
    /*mesh->_setBounds( AxisAlignedBox( Vector3(-r, -r, -r), Vector3(r, r, r) ), false );

    mesh->_setBoundingSphereRadius(r);
    unsigned short src, dest;
    if (!mesh->suggestTangentVectorBuildParams(VES_TANGENT, src, dest))
    {
              mesh->buildTangentVectors(VES_TANGENT, src, dest);
    }*/
}

void Bullet::Update(double deltatime){
    //Check for impact
    if (m_timeToImpact > 0.0){
        this->m_timeToImpact -= deltatime;
        //Kaboom !
        if (m_timeToImpact <= 0.0){
            //decrease Enemy Life
            int life = ((Enemy* )m_enemyToHit)->GetLife();
            life -= ((BasicTower*)m_tower)->GetDamage();
            //remove if target is dead
            if (life <= 0){
                ((Enemy*)m_enemyToHit)->SetLife(life);
                //REFACTORING
                //m_entity_manager->RemoveEntity(m_enemyToHit);
                ((BasicTower*)m_tower)->nearestEnemies.clear();
            }
            else{
                ((Enemy*)m_enemyToHit)->SetLife(life);
                float fRatio = (float)life / ((Enemy*)m_enemyToHit)->GetMaxHealthPoints();
                int newLife = ((Enemy* )m_enemyToHit)->GetLife();
                //visualize decrease scale billboard
                Ogre::Billboard* bBoard = ((Enemy*)m_enemyToHit)->GetBillBoard();
                float fNewWidth = fRatio * 100;
                bBoard->setDimensions(fNewWidth, bBoard->getOwnHeight());
                bBoard->setTexcoordRect(0.0,-1.0,1.0* (fRatio),0.0);
                //Allow Tower to shoot again!
                ((BasicTower*)m_tower)->SetBullet(false);
            }
            //Remove Bullet
            m_entity_manager->RemoveEntity(this);
        }
        else{
            //translate the Bullet here
            Ogre::Vector3 destination = this->m_aimSpot;
            Ogre::Vector3 direction = m_bulletPath;
            Ogre::Real move = speed * deltatime;
            direction.normalise();
            this->GetSceneObject()->translate(move * direction);
        }
    }
}

void Bullet::SetPosition(Ogre::Vector3 pos){
    m_position = pos;
    m_scene_object->setPosition(m_position);
}

void Bullet::SetSource(Ogre::Vector3 position){
    m_towerPos = position;
}

void Bullet::SetDestination(Ogre::Vector3 destination){
    m_enemyPos = destination;
}

void Bullet::PredictIntersect(Ogre::Vector3 enemyVelocity){
    this->m_enemyPos.y += 50.0;
    Ogre::Vector3 destination = this->m_enemyPos;
    Ogre::Vector3 src = this->m_towerPos;
    Ogre::Vector3 direction = destination - src;
    Ogre::Real squaredBulletSpeed = (this->speed * this->speed);

    float a = enemyVelocity.dotProduct(enemyVelocity) - squaredBulletSpeed;
    float b = 2.f*direction.dotProduct(enemyVelocity);
    float c = direction.dotProduct(direction);
    float d = b*b - 4.f*a*c;
    float t = 0.f;
    float t1 = 0.f;
    float t2 = 0.f;

    float p = -b/(2.f*a);
    float q = std::sqrt(d)/(2.f*a);

    t1 = p-q;
    t2 = p+q;

    if (t1 < 0.f && t2 < 0.f){
        t = 0.f;
    }
    if (t1 > t2){
        t = t2;
    }
    else{
        t = t1;
    }

    Ogre::Vector3 aimSpot = destination + enemyVelocity * t;
    //visualize Aiming
    //m_entity_factory->Create(Particle_Type, aimSpot);
    Ogre::Vector3 bulletPath = aimSpot - src;
    this->m_bulletPath = bulletPath;
    this->m_aimSpot = aimSpot;
    m_distance = bulletPath.length();
    m_timeToImpact = m_distance / this->speed;
}

void Bullet::SetEnemy(Entity* enemy){
    this->m_enemyToHit = enemy;
}

void Bullet::SetTower(Entity* tower){
    this->m_tower = tower;
}
