#include "BasicTower.h"
#include "EntityFactory.h"
#include "EntityManager.h"
#include <OgreManualObject.h>
#include <OgreRenderOperation.h>
#include "Bullet.h"
#include "Enemy.h"
#include "math.h"

BasicTower::BasicTower(const EEntityType& entity_type, EntityFactory* entity_factory, EntityManager* entity_manager) : Entity(entity_type){
	m_entity_factory = entity_factory;
	m_entity_manager = entity_manager;
	m_radius = 155;
	m_isShooting = false;
	m_Bullet = false;
	m_animationState = "Idle";
	m_nearestEnemy = 0;
	m_bulletEnt = 0;
	m_despawnAim = Ogre::Vector3(955,0,0);
	m_enemy = 0;
	m_damage = 30;
	m_fireRate = 0.f;
}


BasicTower::~BasicTower(void){

}

void BasicTower::Init(Ogre::SceneManager* scene_manager){
    Ogre::Entity* OgreEnt = scene_manager->createEntity(std::to_string(this->GetId()), this->GetMaterialName());
    m_scene_object = scene_manager->getRootSceneNode()->createChildSceneNode(std::to_string(this->GetId()));
    m_scene_object->yaw(Ogre::Degree(180));
    m_scene_object->attachObject(OgreEnt);
	m_entity_manager->AddEntity(this);

	Ogre::ManualObject* Circle = scene_manager->createManualObject("Circle"+std::to_string(this->GetId()));
    Circle->begin("BaseWhiteNoLighting", Ogre::RenderOperation::OT_TRIANGLE_LIST);

    const float accuracy = 30;
    const float thickness = 2;
    unsigned int index = 0;

    for(float theta = 0; theta <= 2 * Ogre::Math::PI; theta += Ogre::Math::PI / accuracy)
    {
                Circle->position(m_radius * cos(theta),
                                 1,
                                 m_radius * sin(theta));
                Circle->position(m_radius * cos(theta - Ogre::Math::PI / accuracy),
                                 1,
                                 m_radius * sin(theta - Ogre::Math::PI / accuracy));
                Circle->position((m_radius - thickness) * cos(theta - Ogre::Math::PI / accuracy),
                                 1,
                                 (m_radius - thickness) * sin(theta - Ogre::Math::PI / accuracy));
                Circle->position((m_radius - thickness) * cos(theta),
                                 1,
                                 (m_radius - thickness) * sin(theta));
                Circle->quad(index, index + 1, index + 2, index + 3);
                index += 4;
    }

    Circle->end();
    Ogre::SceneNode* CircleNode = m_scene_object->createChildSceneNode("Child"+std::to_string(this->GetId()), this->GetPosition());
    CircleNode->attachObject(Circle);

}

void BasicTower::SetPosition(Ogre::Vector3 pos){
    m_position = pos;
    m_scene_object->setPosition(pos);
}

void BasicTower::AimAtEnemy(Entity* enemy, double timeSinceLastFrame){

    int nextNearestEnemy = enemy->GetId();
    if (m_nearestEnemy == nextNearestEnemy){
        //Shoot if bullet is despawned
        //get distances to enemies in reach of a tower
        //set tower direction to match enemies current position
        m_scene_object = this->GetSceneObject();
        Ogre::Vector3 src = m_scene_object->getOrientation() * Ogre::Vector3::UNIT_SCALE;
        Ogre::Vector3 destination = enemy->GetPosition();
        Ogre::Vector3 direction = destination - m_scene_object->getPosition();
        src.y = 0.0;                                                    // Ignore pitch difference angle
        direction.y = 0.0;
        Ogre::Quaternion quat = src.getRotationTo(direction);
        Ogre::Real distance = direction.normalise();
        m_scene_object->rotate(quat);

        //shoot and damage Enemy
        if (!m_Bullet && m_fireRate<=0.f) {

/*
            this->Shoot(destination, timeSinceLastFrame);
            ((Bullet*) m_bulletEnt)->SetEnemy(enemy);
            ((Bullet*) m_bulletEnt)->SetTower(this);
            m_isShooting = true;
            this->ResetFireRate();
            */i
            this->SetAnimationState("Shoot");

            //std::cout << "I shot: " << this->GetId() << " at: " << enemy->GetId() << " with position: " << enemy->GetPosition() << std::endl;
        }
        else{
            m_isShooting = false;
            m_fireRate -= timeSinceLastFrame;
        }
    }
    else{
        //reaim and shoot
        m_nearestEnemy = nextNearestEnemy;
        m_enemy = enemy;
        //reset firerate because of new aim
        if (m_fireRate > 0){
            m_fireRate = 0.f;
        }
        this->SetBullet(false);

        //get distances to enemies in reach of a tower
        //set tower direction to match enemies current position
        m_scene_object = this->GetSceneObject();

        Ogre::Vector3 src = m_scene_object->getOrientation() * Ogre::Vector3::UNIT_SCALE;
        Ogre::Vector3 destination = enemy->GetPosition();
        Ogre::Vector3 direction = destination - m_scene_object->getPosition();

        src.y = 0.0;
        direction.y = 0.0;
        Ogre::Quaternion quat = src.getRotationTo(direction);
        Ogre::Real distance = direction.normalise();

        std::cout << quat << std::endl;

        m_scene_object->rotate(quat);
        //shoot Enemy and damage it
        if (!m_Bullet && m_fireRate<=0.f) {

/*
            this->Shoot(destination, timeSinceLastFrame);
            ((Bullet*) m_bulletEnt)->SetEnemy(enemy);
            ((Bullet*) m_bulletEnt)->SetTower(this);
            m_isShooting = true;
*/
            this->ResetFireRate();

            this->SetAnimationState("Shoot");

            //std::cout << "I: " << this->GetId() << " shot at: " << enemy->GetId() << " with position: " << enemy->GetPosition() << std::endl;
        }
        else{
            m_isShooting = false;
            m_fireRate -= timeSinceLastFrame;
        }
    }
}

void BasicTower::Shoot(Ogre::Vector3 enemyPos, double timeSinceLastFrame){
    if (!m_isShooting){
        //spawn a single bullet
        Ogre::Vector3 towerPos = this->GetPosition();
        towerPos.y += 60.0;
        m_bulletEnt = m_entity_factory->Create(Bullet_Type, towerPos);
        ((Bullet*) m_bulletEnt)->SetSource(towerPos);
        ((Bullet*) m_bulletEnt)->SetDestination(enemyPos);

        //std::cout << m_despawnAim << "\t" << enemyPos << std::endl;
        Ogre::Vector3 enemyDir = m_despawnAim - enemyPos;
        //std::cout << "EnemyDir: "<< enemyDir << std::endl;
        Ogre::Vector3 normEnemyDir = enemyDir.normalisedCopy();
        //std::cout << "NormEnemyDir: "<< normEnemyDir << std::endl;
        Ogre::Vector3 enemyVelocity = normEnemyDir * ((Enemy*) m_enemy)->GetSpeed();// * 0.1f;
        //std::cout << "enemyVelocity: " << enemyVelocity << std::endl;
        ((Bullet*) m_bulletEnt)->PredictIntersect(enemyVelocity);
        m_Bullet = true;
    }
}

void BasicTower::SetAnimationState(char* animationstate){
    m_animationState = animationstate;
}

bool BasicTower::HasBullet(){
    return m_Bullet;
}

void BasicTower::Update(double deltatime){

    //Only manage Animation state
    Ogre::Entity* OgreEnt = (Ogre::Entity*)this->GetSceneObject()->getAttachedObject(Ogre::StringConverter::toString(this->GetId()));
    Ogre::AnimationState* animationState = OgreEnt->getAnimationState(m_animationState);
    //std::cout << "AnimationName: " << animationState->getAnimationName() << std::endl;
    animationState->setEnabled(true);
    if (OgreEnt->getAnimationState("Shoot")->hasEnded()) {
        this->SetAnimationState("Idle");
        animationState->setLoop(true);
    }
    if(m_animationState == "Shoot"){
        animationState->setLoop(false);
    }
    animationState->addTime(deltatime / 1000.0f);
}


void BasicTower::SetBullet(bool boolean){
    this->m_Bullet = boolean;
}


int BasicTower::GetDamage(){
    return this->m_damage;
}

void BasicTower::ResetFireRate(){
    this->m_fireRate = 1000.f;
}
