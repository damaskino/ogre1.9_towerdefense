#include "Border.h"
#include "EntityFactory.h"
#include "EntityManager.h"
#include "GameState.h"

Border::Border(const EEntityType& entity_type, EntityFactory* entity_factory, EntityManager* entity_manager) : Entity(entity_type){
	m_entity_factory = entity_factory;
	m_entity_manager = entity_manager;
    int degree = 0;
}

Border::~Border(void)
{

}

void Border::Init(Ogre::SceneManager* scene_manager){
    Ogre::Entity* pEntity = scene_manager->createEntity(std::to_string(this->GetId()), this->resource_name);
    pEntity->setQueryFlags(BORDER_ENTITY);
    pEntity->setMaterialName("Examples/Rockwall");
    Ogre::SceneNode* Border1 = scene_manager->getRootSceneNode()->createChildSceneNode(std::to_string(this->GetId()));//
    Border1->attachObject(pEntity);
    Border1->setPosition(this->m_position);
    Border1->yaw(Ogre::Degree(this->degree));
}

void Border::Update(double deltatime){

}

void Border::SetPosition(Ogre::Vector3 pos){
    m_position = pos;
}
