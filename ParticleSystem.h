#ifndef PARTICLESYSTEM_H
#define PARTICLESYSTEM_H

#include <OgreSceneManager.h>
#include <OgreLogManager.h>
#include <OgreParticleEmitter.h>
#include <OgreParticleSystem.h>

class MyParticleSystem
{
    public:
        /** Default constructor */
        MyParticleSystem(Ogre::SceneManager* scene_manager, Ogre::Log* logger);
        /** Default destructor */
        virtual ~MyParticleSystem();
        Ogre::ParticleSystem* GetParticleSystem();

    protected:
    private:
        Ogre::SceneManager* m_scene_manager;
        Ogre::Log* m_logger;
        Ogre::ParticleSystem* m_particle_system;
};

#endif // PARTICLESYSTEM_H
