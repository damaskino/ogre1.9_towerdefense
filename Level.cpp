#include "Level.h"
#include "OgreMeshManager.h"
#include "OgreSceneManager.h"
#include "OgreEntity.h"
#include "GameState.h"
#include "OgreSceneNode.h"
#include "Border.h"
#include "BorderBuilder.h"
#include "Enemy.h"
#include <cmath>
#include <utility>


int entity_count = 0;
std::vector<Ogre::Vector3> startingPath;


Level::Level(Ogre::SceneManager* smPtr, Ogre::MeshManager* mmPtr, Ogre::String materialName, EntityManager* m_entity_manager, EntityFactory* m_entity_factory, EnemyLevelMediator* m_enemy_level_mediator)
{

    std::cout << "Building level..." << std::endl;

    //For Pathplanning
    m_pEnemyLevelMediator = m_enemy_level_mediator;

    m_Wave             = 1;
    m_enemySeconds      = 1;
    m_spawnComplete     = false;


    Grid::mAttachedNode = 0;
    Grid::material = materialName;
    m_pMeshMgr = mmPtr;
    m_pSceneMgr = smPtr;
    m_pEntityManager = m_entity_manager;
    m_pEntityFactory = m_entity_factory;

    //grid defaults
    Grid::mOwnNode = m_pSceneMgr->createSceneNode();
    Grid::mGrid = m_pSceneMgr->createManualObject();
    Grid::mGrid->setDynamic(true);
    Grid::mOwnNode->attachObject(Grid::mGrid);

    //now set the default settings, the default grid will be xy plane, -5 to +5 with 10 divisions
    Grid::drawQueue = 1;    //assuming that Background is not always the best place
    Grid::csX = Grid::csY = Grid::csZ = 1;    //cell sizes
    Grid::ofX = Grid::ofY = Grid::ofZ = 0;    //offsets
    Grid::xmin = Grid::ymin = Grid::zmin = -5;
    Grid::xmax = Grid::ymax = Grid::zmax = 5;
    Grid::showXY = true;
    Grid::showXZ = Grid::showYZ = false;
    Grid::colorOffset = Ogre::ColourValue(0,0,0,1);
    Grid::colorMaster = Ogre::ColourValue(0,0,0,0.4);
    Grid::colorDivision = Ogre::ColourValue(0,0,0,0.1);
    Grid::showDivisions = true;
    Grid::divX = Grid::divY = Grid::divZ = 10;

    //An array representation of the grid for path planning
    //It would be nice if the dimensions of this array could be determined dynamically according to the actual grid
    //Cells can be either free or blocked
    //Populate the grid cells
    for (int i = 0; i<32; i++){
        for (int j = 0; j<32; j++){
                    m_GridArray[i][j] = 0.0;

        }
    }

    //left top
    fillGridBlock(5, 12, 5, 12);
    //left bottom
    fillGridBlock(21, 28, 5,12 );
    //right top
    fillGridBlock(5,12, 21,28);
    //right bottom
    fillGridBlock(21,28, 21,28);


}

void Level::fillGridBlock(int xMin, int xMax, int yMin, int yMax){
    for (int i = xMin-1; i<xMax; i++){

        for (int j = yMin-1; j<yMax; j++){
            m_GridArray[i][j]=INFINITY;
        }
    }
}


void Level::updateMediator(){

    for (int i = 0; i<32; i++){
            for (int j = 0; j<32; j++){
                   m_pEnemyLevelMediator->m_GridArray[i][j] = m_GridArray[i][j];
            }
    }
}

//Check if path to goal is sealed off, if the given coordinate becomes untraversable
bool Level::goalBlocked(int towerCol, int towerRow){

    //Build temporary Array
    float tmpGrid[32][32];
    for (int i = 0; i <= 31; i++){
        for (int j = 0; j<= 31; j++){
                  tmpGrid[i][j] = m_GridArray[i][j];
        }
    }

    //Block the array where the tower would be placed
    tmpGrid[towerCol][towerRow] = INFINITY;

    std::vector<std::pair<int,int>> frontier;
    frontier.push_back(std::make_pair(15 , 31));
    float distance = 0.0;

    while (!frontier.empty()){

        std::pair<int,int> q = frontier.back();
        if (q.first == 15 && q.second == 0){
            std::cout << "Path to goal is not blocked." << std::endl;
            return false;
        }

        frontier.pop_back();
        distance++;
        //Check for neighbours
        for(int i = q.first-1; i<= q.first+1; i++){

                if (i == q.first){   continue;}
                if (tmpGrid[i][q.second] > 0){  continue;}
                if (i>=32){          continue;}
                if (i<0){            continue;}

                tmpGrid[i][q.second] = distance;
                frontier.push_back(std::make_pair(i, q.second));
        }

        for(int i = q.second-1; i<= q.second+1; i++){

                if (i == q.second){   continue;}
                if (tmpGrid[q.first][i] > 0){  continue;}
                if (i>=32){           continue;}
                if (i<0){             continue;}

                tmpGrid[q.first][i] = distance;
                frontier.push_back(std::make_pair(q.first, i));
        }
    }

    std::cout << "Path is blocked." << std::endl;
    return true;
}


//TODO: for later optimization, use the same resource if a plane has the same size
void Level::buildPlane(int wallHeight, int width, int height, Ogre::Vector3 pos, Ogre::Vector3 vec, int degree){
    Entity* bEntity = this->m_pEntityFactory->Create(Border_Type, pos);
    //call Builder
    BorderBuilder* bBuilder = new BorderBuilder();
    //Build complex concrete object from here
    //the Builder takes the entity and the parameter to Init it
    //call entity->Init from Builder return here after Building
    bBuilder->BuildPart(m_pSceneMgr, m_pMeshMgr, degree, height, width, wallHeight, vec, bEntity);
    if (entity_count == 0){
        this->m_pEntityManager->AddEntity(bEntity);
        entity_count = 5;
    }
    entity_count --;
}


void Level::buildBlock(int width, int height, Ogre::Vector3 pos){
      //fix height  (Deckel ist 100 units ueber dem Boden)
      int wallHeight = 100;
      //Deckel
      buildPlane(wallHeight, width, height, pos, Ogre::Vector3::UNIT_Y, 0);
      //degree 0 front and back
      //front
      pos.x += 250;
      buildPlane(wallHeight, width, height, pos, Ogre::Vector3::UNIT_X, 0);
      //back
      pos.x -= 500;
      buildPlane(wallHeight, width, height, pos, Ogre::Vector3::UNIT_X, 0);
      //left
      //reset x position to 0
      pos.x +=250;
      pos.z -= 250;
      buildPlane(wallHeight, width, height, pos, Ogre::Vector3::UNIT_X, -90);
      //right
      pos.z += 500;
      buildPlane(wallHeight, width, height, pos, Ogre::Vector3::UNIT_X, -90);
}


void Level::createLevel()
{
    m_pEntityFactory->Create(Particle_Type, Ogre::Vector3(1000, 0, -31.25));
    m_pEntityFactory->Create(Particle_Type, Ogre::Vector3(-1000, 0, -31.25));
    Ogre::Vector3 coord = Ogre::Vector3(-500,0,500);
    buildBlock(500, 500, coord);

    coord = Ogre::Vector3(500,0,500);
    buildBlock(500, 500, coord);

    coord = Ogre::Vector3(-500,0,-500);
    buildBlock(500, 500, coord);

    coord = Ogre::Vector3(500,0,-500);
    buildBlock(500, 500, coord);

}


void Level::attachToNode(Ogre::SceneNode* node){
    //if already attached, deattach it first
    if(Grid::mAttachedNode)Grid::mAttachedNode->removeChild(Grid::mOwnNode);
    //attach to the new node
    Grid::mAttachedNode = node;
    node->addChild(Grid::mOwnNode);
}


void Level::update(){
    //Generate the grid mesh
    Ogre::ManualObject* o = Grid::mGrid;

    o->setQueryFlags(GRID_ENTITY);

    OgreFramework::getSingletonPtr()->m_pLog->logMessage("Flags of Grid: "+std::to_string(o->getQueryFlags()));

    //set the render queue
    o->setRenderQueueGroup(Grid::drawQueue);

    //offset and cell size
    Grid::mOwnNode->setPosition(ofX,ofY,ofZ);
    Grid::mOwnNode->setScale(csX,csY,csZ);

    OgreFramework::getSingletonPtr()->m_pLog->logMessage("Name of Grid: "+Grid::mOwnNode->getName());

    Ogre::ColourValue* cl;
    Ogre::Real r;

    //Clear the manual object to completely regenerate it
    o->clear();
    o->begin(Grid::material, Ogre::RenderOperation::OT_LINE_LIST);
    if(Grid::showXY)
    {
        //major lines
        //X
        for(int x=xmin;x<=xmax;++x)
        {
            if(x==0) cl = &colorOffset;
            else cl = &colorMaster;
            o->position(x,ymin,0);
            o->colour(*cl);
            o->position(x,ymax,0);
            o->colour(*cl);
            //divisions
            if(x<xmax && showDivisions && divX)
            {
                for(int d=1;d<divX;++d)
                {
                    r = x+ (1.0/Ogre::Real(divX))*Ogre::Real(d);
                    o->position(r,ymin,0);
                    o->colour(colorDivision);
                    o->position(r,ymax,0);
                    o->colour(colorDivision);
                }
            }
        }
        //Y
        for(int y=ymin;y<=ymax;++y)
        {
            if(y==0) cl = &colorOffset;
            else cl = &colorMaster;
            o->position(xmin,y,0);
            o->colour(*cl);
            o->position(xmax,y,0);
            o->colour(*cl);
            //divisions
            if(y<ymax && showDivisions && divY)
            {
                for(int d=1;d<divY;++d)
                {
                    r = y+ (1.0/Ogre::Real(divY))*Ogre::Real(d);
                    o->position(xmin,r,0);
                    o->colour(colorDivision);
                    o->position(xmax,r,0);
                    o->colour(colorDivision);
                }
            }
        }
    }

    if(showXZ)
    {
        //major lines
        //X
        for(int x=xmin;x<=xmax;++x)
        {
            if(x==0) cl = &colorOffset;
            else cl = &colorMaster;
            o->position(x,0,zmin);
            o->colour(*cl);
            o->position(x,0,zmax);
            o->colour(*cl);
            //divisions
            if(x<xmax && showDivisions)
            {
                for(int d=0;d<divX;++d)
                {
                    r = x+ (1.0/Ogre::Real(divX))*Ogre::Real(d);
                    o->position(r,0,zmin);
                    o->colour(colorDivision);
                    o->position(r,0,zmax);
                    o->colour(colorDivision);
                }
            }
        }

        //Z
        for(int z=zmin;z<=zmax;++z)
        {
            if(z==0) cl = &colorOffset;
            else cl = &colorMaster;
            o->position(xmin,0,z);
            o->colour(*cl);
            o->position(xmax,0,z);
            o->colour(*cl);
            if(z<zmax && showDivisions)
            {
                //divisions
                for(int d=0;d<divZ;++d)
                {
                    r = z+ (1.0/Ogre::Real(divZ))*Ogre::Real(d);
                    o->position(xmin,0,r);
                    o->colour(colorDivision);
                    o->position(xmax,0,r);
                    o->colour(colorDivision);
                }
            }
        }
    }

    //YZ
    if(showYZ)
    {
        //major lines
        //Y
        for(int y=ymin;y<=ymax;++y)
        {
            if(y==0) cl = &colorOffset;
            else cl = &colorMaster;
            o->position(0,y,zmin);
            o->colour(*cl);
            o->position(0,y,zmax);
            o->colour(*cl);
            //divisions
            if(y<ymax && showDivisions)
            {
                for(int d=0;d<divY;++d)
                {
                    r = y+ (1.0/Ogre::Real(divY))*Ogre::Real(d);
                    o->position(0,r,zmin);
                    o->colour(colorDivision);
                    o->position(0,r,zmax);
                    o->colour(colorDivision);
                }
            }
        }

        //Z
        for(int z=zmin;z<=zmax;++z)
        {
            if(z==0) cl = &colorOffset;
            else cl = &colorMaster;
            o->position(0,ymin,z);
            o->colour(*cl);
            o->position(0,ymax,z);
            o->colour(*cl);
            //divisions
            if(z<zmax && showDivisions)
            {
                for(int d=0;d<divZ;++d)
                {
                    r = z+ (1.0/Ogre::Real(divZ))*Ogre::Real(d);
                    o->position(0,ymin,r);
                    o->colour(colorDivision);
                    o->position(0,ymax,r);
                    o->colour(colorDivision);
                }
            }
        }
    }
    o->end();
}


void Level::hide(){
    Grid::mOwnNode->setVisible(false);
}


void Level::show(){
    Grid::mOwnNode->setVisible(true);
}


//For Debugging
void Level::printGrid(){

    for(int row=31; row >= 0; row--){
        std::cout << std::endl;
        for(int col=0; col < 32; col++){
            if (m_GridArray[col][row] == 0){
                std::cout << "  ";
            }
            std::cout << m_GridArray[col][row];
        }
    }
    std::cout << std::endl;
}


//belassen wir weltkoordinate in 0/0/0 so dass GridZelle unten links = -32/-32 ist
std::tuple<int, int> Level::posInGridCell(double x, double z, double unit){
    double xFromCenter = x;
    double zFromCenter = z;
    int col = std::floor(zFromCenter/unit);
    int row = std::floor(xFromCenter/unit);
    //return "Col:"+std::to_string(col)+", Row:"+std::to_string(row)+", xFC:"+std::to_string(xFromCenter)+", zFC:"+std::to_string(zFromCenter);
    return std::tuple<int, int>(col, row);
}

// remapping von world coordinate 0/0/0 nach unten links, so dass GridZelle unten links = 0/0 ist
//unit is 62.5
std::tuple<int, int> Level::posInGrid(double x, double z, double unit){
    double xFromCenter = x;
    double zFromCenter = z;
    int col = std::floor((zFromCenter-(-1032.5))/unit);
    int row = std::floor((xFromCenter-(-1032.5))/unit);
    //return "Col:"+std::to_string(col)+", Row:"+std::to_string(row)+", xFC:"+std::to_string(xFromCenter)+", zFC:"+std::to_string(zFromCenter);
    return std::tuple<int, int>(col-1, row-1);
}


//Everything related to Enemies in the level
void Level::setTimer(){
    m_pWaveTimer = new Ogre::Timer();
    m_pEnemyTimer = new Ogre::Timer();
    m_pWaveTimer->reset();
    m_pEnemyTimer->reset();
}

bool Level::startWave(){
    if (m_pWaveTimer->getMilliseconds() > 5000){
        m_pWaveTimer->reset();
        return true;
    }
    else{
        return false;
    }
}

void Level::spawnEnemy(Ogre::Vector3 pos){
    if (m_pEnemyTimer->getMilliseconds() >= 6000){
            m_enemySeconds -= 1;

            if (m_enemySeconds >= 0){
                m_spawnComplete = false;
                Entity* enemy = m_pEntityFactory->Create(Enemy_Type, pos);
                int newHP = 110 + std::ceil(((float)this->m_Wave/10) * 600);
                ((Enemy*)enemy)->SetMaxHealthPoints(newHP);
                ((Enemy*)enemy)->SetLife(newHP);

                std::vector<Ogre::Vector3> myPath = m_pEnemyLevelMediator->getCurrentPath();
                ((Enemy*)enemy)->SetPath(&myPath);
            }
            else{
                //m_enemySeconds++;
                m_spawnComplete = true;
            }
            m_pEnemyTimer->reset();
    }
}


bool Level::GetSpawnComplete(){
    return m_spawnComplete;
}


void Level::ResetSpawnComplete(){
    m_spawnComplete = false;
}


void Level::AddWave(){
    m_Wave++;
    if (m_Wave > 20){
        m_enemySeconds = 20;
    }
    else{
        m_enemySeconds = m_Wave;
    }
}


int Level::GetWave(){
    return m_Wave;
}


bool Level::WaveOver(){
    int nEnemies = 0;
    nEnemies = m_pEntityManager->countEnemies();
    if (nEnemies > 0){
        return false;
    }
    else{
        return true;
    }

}
