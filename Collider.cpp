#include "Collider.h"
#include "BasicTower.h"
#include <OgreAxisAlignedBox.h>
#include <OgreMovableObject.h>
#include <OgreSphere.h>
#include "Enemy.h"

Collider::Collider(EntityManager* entity_manager, EnemyLevelMediator* enemy_level_mediator) : CollisionHandler(), CollisionListener()
{
    m_enemy_tower_mediator = new EnemyTowerMediator(entity_manager);
    m_enemy_level_mediator = enemy_level_mediator;
    m_entity_manager = entity_manager;
    countCollision = 0;
    hashChecks = 0;
}

Collider::~Collider()
{
    //dtor
}

void Collider::Resolve(CollisionTypes type, double timeSinceLastFrame, Entity* ent1, Entity* ent2){
    //According to collisiontypes call mediator and pathplanning here
    if (type == Enemy_Level){
        m_enemy_level_mediator->planGeneralPath();
    }

    if (type == Tower_Enemy){

        std::cout << "Tower Enemy collision detected!" << std::endl;

        //Set back enemies position slightly, to avoid segfault due to further collisions during pathplanning
        Enemy* pEn = (Enemy*) ent1;
        Ogre::Vector3 diffVec = pEn->GetLastPosition()-pEn->GetPosition();
        diffVec = diffVec*0.01;
        pEn->SetPosition(diffVec+pEn->GetPosition());
        pEn->TurnBack();

        //New path for the enemy from the last position (before collision) to the goal.
        Ogre::Vector3 lastPos = pEn->GetLastPosition();
        std::tuple<int, int> lastColRow = m_entity_manager->posInGrid(lastPos[0],lastPos[2], 62.5);
        std::cout << "Last Coordinates: " << std::endl;
        int lastCol = std::get<0>(lastColRow);
        int lastRow = std::get<1>(lastColRow);
        std::vector<Ogre::Vector3> singlePath =  m_enemy_level_mediator->planSinglePath(lastCol, lastRow);
        pEn->SetPath(&singlePath);

        //New path from start to goal for all enemies that spawn afterwards.
        m_enemy_level_mediator->planGeneralPath();
    }

    if (type == Enemy_TowerRadius){
        m_enemy_tower_mediator->addAndSortByEuclidianDistance(ent1, ent2);
        Entity* nearestEnemy = m_enemy_tower_mediator->getNearestEnemy(ent2);
        ((BasicTower*)ent2)->AimAtEnemy(nearestEnemy, timeSinceLastFrame);
    }
}


//Listener lets Resolve check for Collision, and returns true if a collision was detected
//This is our broad phase and narrow phase or brute force collision detection
void Collider::Listen(double timeSinceLastFrame){
    //Check for collisions and call handle
    //std::cout << "Checking for collisions" << std::endl;

    //get all entities
    size_t num_entities = m_entity_manager->GetEntities();

    //std::cout << "size of m_entities: " << num_entities << std::endl;
    //if size is smaller or only one entity exist we dont have to check for collisions
    if (num_entities > 1){
        /*
        //size_t coll_entities [num_entities];

        //Old Brute Force Approach
        for(int i=0; i<num_entities; i++){

            //std::cout << "Checking first entity" << std::endl;
            //std::cout << i << std::endl;
            Entity* ent1 = m_entity_manager->GetEntity(i);


            for(int j=0; j<num_entities; j++){

                //std::cout << "Checking second entity" << std::endl;
                //std::cout << j << std::endl;

                //check entities won't collide with themselves
                if (i==j){
                    //std::cout << "Skipping:" << std::endl;
                    //std::cout << i << std::endl;
                    //std::cout << j << std::endl;
                    continue;
                }

                //std::cout << "Second entity checked" << std::endl;
                //get second entity
                Entity* ent2 = m_entity_manager->GetEntity(j);

                //candidates are chosen now we check narrow phase and resolve the collisions

                //std::cout << ent1->GetId() << "\t" << ent2->GetId() << std::endl;
                //std::cout<< "Checking for Collision between entities:" << std::endl;
                //check collision
        */

        //instead of brute force we want to use a broad phase collision detection based on the grid
        std::vector<std::vector<std::vector<Entity*>>> spatialGrid = m_entity_manager->UpdateSpatialGrid();

        //TODO algorithm for broad phase using spatial Grid
        std::vector<std::vector<Entity*>> Pairs = this->queryForCollisionPairs(spatialGrid);

        for (unsigned int i; i < Pairs.size(); i++){
            Entity* ent1 = Pairs.at(i)[0];
            Entity* ent2 = Pairs.at(i)[1];

            //std::cout << ent1->GetId() << "\t" << ent2->GetId() << std::endl;

            if( this->SphereCubeCollision(ent1, ent2, timeSinceLastFrame) ){
                //ent1 is Enemy ent2 is tower, check is in SphereCubeCollision()
                //Tower enemy collision detected
                //call handle with Type
                //std::cout << "Enemy TowerRadius Collision Type detected" << std::endl;
                this->Resolve(CollisionTypes::Enemy_TowerRadius, timeSinceLastFrame, ent1, ent2);
            }
            if( this->CubeCubeCollision(ent1, ent2) ){
                //Tower enemy collision detected -> Pathplanning
                std::cout << "Tower Enemy Collision Type detected" << std::endl;
                //call handle with Type
                this->Resolve(CollisionTypes::Tower_Enemy, timeSinceLastFrame, ent1, ent2);
            }
            if( this->WallCubeCollision(ent1, ent2) ){
                //level enemy collision detected
                std::cout << "Enemy Level Collision Type detected" << std::endl;
                this->Resolve(CollisionTypes::Enemy_Level, timeSinceLastFrame, ent1, ent2);
            }
        }
    }
}


bool Collider::SphereCubeCollision(Entity* ent1, Entity* ent2, double timeSinceLastFrame){
    if (ent1->GetType()==Enemy_Type && ent2->GetType()==Basic_Tower)
    {
        //Enemy Box
        const Ogre::AxisAlignedBox bBox =  ent1->GetSceneObject()->_getWorldAABB();

        Ogre::SceneNode* CircleNode = (Ogre::SceneNode*)ent2->GetSceneObject()->getChild("Child"+std::to_string(ent2->GetId()));

        //Tower Bounding Sphere
        const Ogre::Sphere bSphere = CircleNode->getAttachedObject("Circle"+std::to_string(ent2->GetId()))->getWorldBoundingSphere(false);

        if (bSphere.intersects(bBox)){
            return true;
        }

        else {
            return false;
        }
    }
    return false;
}

bool Collider::CubeCubeCollision(Entity* ent1, Entity* ent2){
    if (ent1->GetType()==Enemy_Type && ent2->GetType()==Basic_Tower)
    {
        //Enemy Box
        const Ogre::AxisAlignedBox bBoxE =  ent1->GetSceneObject()->_getWorldAABB();

        //Tower Bounding Box
        const Ogre::AxisAlignedBox bBoxT =  ent2->GetSceneObject()->_getWorldAABB();

        if (bBoxT.intersects(bBoxE)){
            ent2->GetSceneObject()->showBoundingBox(true);
            return true;
        }

        else {
            ent2->GetSceneObject()->showBoundingBox(false);
            return false;
        }
    }
    return false;
}

bool Collider::WallCubeCollision(Entity* ent1, Entity* ent2){
    if (ent1->GetType()==Enemy_Type && ent2->GetType()==Border_Type)
    {
        //Enemy Box
        const Ogre::AxisAlignedBox bBoxE =  ent1->GetSceneObject()->_getWorldAABB();

        //Border Bounding Box
        const Ogre::AxisAlignedBox bBoxW =  ent2->GetSceneObject()->_getWorldAABB();

        if (bBoxW.intersects(bBoxE)){
            return true;
        }

        else {
            return false;
        }
    }
    return false;
}

std::vector<std::vector<Entity*>> Collider::queryForCollisionPairs(std::vector<std::vector<std::vector<Entity*>>> spatialGrid){

    std::map<std::string, bool> checked;
    std::vector<std::vector<Entity*>> Pairs;
    this->countCollision = 0;
    this->hashChecks = 0;

    for (int i=0; i < spatialGrid.size(); i++){
        std::vector<std::vector<Entity*>> gridCol = spatialGrid.at(i);
        if (gridCol.empty()){
            continue;
        }
        for(int j = 0; j < gridCol.size(); j++){
            std::vector<Entity*> gridCell = gridCol.at(j);
            if (gridCell.empty()){
                continue;
            }
            for(int k = 0; k < gridCell.size(); k++){
                Entity* ent1 = gridCell.at(k);

                for(int l = 0; l < gridCell.size(); l++){
                    Entity* ent2 = gridCell.at(l);
                    if (l == k){
                        continue;
                    }

                    if (ent1->GetType() != Enemy_Type){
                        continue;
                    }

                    std::string hashA = ent1->GetId() + ":" + ent2->GetId();
                    std::string hashB = ent2->GetId() + ":" + ent1->GetId();

                    this->hashChecks += 2;
                    std::map<std::string, bool>::iterator itA = checked.find(hashA);
                    std::map<std::string, bool>::iterator itB = checked.find(hashB);
                    if (itA == checked.end() && itB == checked.end()){
                        checked[hashA] = true;
                        checked[hashB] = true;
                        this->countCollision ++;
                        std::vector<Entity*> apair {ent1, ent2};
                        Pairs.push_back( apair );
                    }
                }
            }
        }
    }
    return Pairs;
}
