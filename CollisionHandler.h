#ifndef COLLISIONHANDLER_H
#define COLLISIONHANDLER_H

class Entity;

enum CollisionTypes
{
    Enemy_TowerRadius = 1 << 0,
    Enemy_Level = 1 << 1,
    Tower_Enemy = 1 << 2
};

class CollisionHandler
{
    public:
        CollisionHandler(){
        }
        virtual ~CollisionHandler(){}
        //resolve collisions into cases:
        virtual void Resolve(CollisionTypes type, double timeSinceLastFrame, Entity* ent1, Entity* ent2)=0; //call the mediator according to collision type described by type of collision (overload)

    protected:
    private:


};

#endif // CollisionHANDLER_H


