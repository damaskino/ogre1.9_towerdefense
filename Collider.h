#ifndef COLLIDER_H
#define COLLIDER_H

#include "CollisionHandler.h"
#include "CollisionListener.h"
#include "EnemyTowerMediator.h"
#include "EnemyLevelMediator.h"

class EntityManager;
class Entity;

class Collider : public CollisionHandler, public CollisionListener
{
    public:
        Collider(EntityManager* entity_manager, EnemyLevelMediator* m_enemy_level_mediator);
        virtual ~Collider();
        void Listen(double timeSinceLastFrame);
        void Resolve(CollisionTypes type, double timeSinceLastFrame, Entity* ent1, Entity* ent2);
        bool SphereCubeCollision(Entity* ent1, Entity* ent2, double timeSinceLastFrame);
        bool CubeCubeCollision(Entity* ent1, Entity* ent2);
        bool WallCubeCollision(Entity* ent1, Entity* ent2);
        std::vector<std::vector<Entity*>> queryForCollisionPairs(std::vector<std::vector<std::vector<Entity*>>> spatialGrid);
    protected:
    private:
        EnemyTowerMediator*     m_enemy_tower_mediator;
        EnemyLevelMediator*     m_enemy_level_mediator;
        EntityManager*          m_entity_manager;
        int                     countCollision;
        int                     hashChecks;

};
#endif // COLLIDER_H
