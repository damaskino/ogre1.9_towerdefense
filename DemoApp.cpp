#include "DemoApp.h"

#include "MenuState.h"
#include "GameState.h"
#include "PauseState.h"
#include "GameOverState.h"

DemoApp::DemoApp()
{
	m_pAppStateManager = 0;
}


DemoApp::~DemoApp()
{
	delete m_pAppStateManager;
    delete OgreFramework::getSingletonPtr();
}


void DemoApp::startDemo()
{
	new OgreFramework();
	if(!OgreFramework::getSingletonPtr()->initOgre("AdvancedOgreFramework", 0, 0))
		return;

	OgreFramework::getSingletonPtr()->m_pLog->logMessage("Demo initialized!");

	m_pAppStateManager = new AppStateManager();

	MenuState::create(m_pAppStateManager, "MenuState");
	GameState::create(m_pAppStateManager, "GameState");
    PauseState::create(m_pAppStateManager, "PauseState");
    GameOverState::create(m_pAppStateManager, "GameOverState");


	m_pAppStateManager->start(m_pAppStateManager->findByName("MenuState"));
}
