#ifndef ENTITYFACTORY_H
#define ENTITYFACTORY_H

#include "Entity.h"
#include <OgreBillboardSet.h>

class EntityManager;
class SceneManager;
class MyParticleSystem;
class EntityFactory
{
public:
    EntityFactory(Ogre::SceneManager* scene_manager, MyParticleSystem* m_PS);
    virtual ~EntityFactory(void);
    Entity* Create(EEntityType entity_type, Ogre::Vector3 pos);
    void SetEntityManager(EntityManager* entity_manager) { m_entity_manager = entity_manager; }
private:
    Ogre::SceneManager* m_scene_manager;
	EntityManager* m_entity_manager;
	MyParticleSystem* m_particle_system;
};

#endif // ENTITYFACTORY_H
