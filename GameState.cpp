#include "GameState.h"
#include "EntityManager.h"
#include "EntityFactory.h"
#include "ParticleSystem.h"
#include "Collider.h"
#include "Player.h"
#include <fstream>

using namespace Ogre;

GameState::GameState() : m_entity_factory(NULL), m_entity_manager(NULL), m_laserParticle(NULL)
{
    m_MoveSpeed			= 1.0f;
    m_RotateSpeed		= 3.0f;

    m_bLMouseDown       = false;
    m_bRMouseDown       = false;
    m_bQuit             = false;
    m_bSettingsMode     = false;

    m_pDetailsPanel		= 0;
    m_numberOfTowers = 0;
    m_seconds = 5;
    spawnable = false;
}

static bool entity_factory_created = false;
//int Player::m_pLife = 2;
int Player::m_pLife = 100;

int Player::m_pGold = 200000;
//int Player::m_pGold = 40;
int Player::m_killCount = 0;


void GameState::enter()
{
    OgreFramework::getSingletonPtr()->m_pLog->logMessage("Entering GameState...");

    m_pSceneMgr = OgreFramework::getSingletonPtr()->m_pRoot->createSceneManager(ST_GENERIC, "GameSceneMgr");
    Ogre::Log* logger = OgreFramework::getSingletonPtr()->m_pLog;
    m_entity_manager = new ::EntityManager(m_pSceneMgr, logger);
    OgreFramework::getSingletonPtr()->m_pLog->logMessage("Try building ParticleSystem...");
    m_laserParticle = new ::MyParticleSystem(m_pSceneMgr, logger);
    m_pSceneMgr->setAmbientLight(Ogre::ColourValue(0.7f, 0.7f, 0.7f));
    m_pSceneMgr->addRenderQueueListener(OgreFramework::getSingletonPtr()->m_pOverlaySystem);
    m_pRSQ = m_pSceneMgr->createRayQuery(Ray());
    m_pRSQ->setQueryMask(GROUND_ENTITY | BORDER_ENTITY);

    m_pCamera = m_pSceneMgr->createCamera("GameCamera");
    m_pCamera->setPosition(Vector3(-500, 500, 0));
    m_pCamera->lookAt(Vector3(0, 0, 0));
    m_pCamera->setNearClipDistance(5);

    m_pCamera->setAspectRatio(Real(OgreFramework::getSingletonPtr()->m_pViewport->getActualWidth()) /
        Real(OgreFramework::getSingletonPtr()->m_pViewport->getActualHeight()));

    OgreFramework::getSingletonPtr()->m_pViewport->setCamera(m_pCamera);
    SceneNode* camNode = m_pSceneMgr->getRootSceneNode()->createChildSceneNode("cameraNode");
    SceneNode* camFacingNode = m_pSceneMgr->getSceneNode("cameraNode")->createChildSceneNode("cameraFacingNode");
    camFacingNode->attachObject(m_pCamera);
    m_pCurrentObject = 0;
    m_pCTime = new Ogre::Timer();
    m_pCTime->reset();
    m_pIncomeTimer = new Ogre::Timer();
    m_pIncomeTimer->reset();

    if (!entity_factory_created)
	{
		m_entity_factory = new ::EntityFactory(m_pSceneMgr, m_laserParticle);
		entity_factory_created = true;
		m_entity_factory->SetEntityManager(m_entity_manager);
	}


    buildGUI();

    createScene();
}


bool GameState::pause()
{
    OgreFramework::getSingletonPtr()->m_pLog->logMessage("Pausing GameState...");

    return true;
}


void GameState::resume()
{
    OgreFramework::getSingletonPtr()->m_pLog->logMessage("Resuming GameState...");

    buildGUI();

    OgreFramework::getSingletonPtr()->m_pViewport->setCamera(m_pCamera);
    m_bQuit = false;
}


void GameState::exit()
{
    OgreFramework::getSingletonPtr()->m_pLog->logMessage("Leaving GameState...");
    m_pSceneMgr->destroyCamera(m_pCamera);
    m_pSceneMgr->destroyQuery(m_pRSQ);
    Player::m_pLife = 100;

    //Player::m_pGold = 40;
    Player::m_pGold = 20000000;
    m_pIncomeTimer->~Timer();
    m_pCTime->~Timer();
    m_pCurrentObject = 0;
    m_seconds = 5;
    m_laserParticle->~MyParticleSystem();
    m_collider->~Collider();
    m_entity_factory->~EntityFactory();
    m_pLevel->~Grid();
    m_Player->~Player();
    entity_factory_created = false;
    spawnable = false;
    if(m_pSceneMgr)
        OgreFramework::getSingletonPtr()->m_pRoot->destroySceneManager(m_pSceneMgr);
}


void GameState::createScene()
{
    m_pSceneMgr->createLight("Light")->setPosition(75,75,75);
    Ogre::ColourValue fadeColour(0.1, 0.1, 0.1);
    OgreFramework::getSingletonPtr()->m_pViewport->setBackgroundColour(fadeColour);
    m_pSceneMgr->setFog(Ogre::FOG_LINEAR, fadeColour, 0.0, 500, 2500);
    //sky
    //m_pSceneMgr->setSkyBox(true, "Examples/MyAlien", 3500, false);

    //ground plane
    Ogre::Plane plane (Ogre::Vector3::UNIT_Y, 0);
    //ground plane width height
    Ogre::MeshManager::getSingleton().createPlane("ground", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, plane, 7000, 7000, 20, 20, true, 1, 5, 5, Ogre::Vector3::UNIT_Z);
    //register planes as entities
    Ogre::Entity* entGround = m_pSceneMgr->createEntity("GroundEntity", "ground");
    //make ground queryable for raycast
    entGround->setQueryFlags(GROUND_ENTITY);
    //ground node SceneNode
    SceneNode* GroundNode = m_pSceneMgr->getRootSceneNode()->createChildSceneNode();
    GroundNode->attachObject(entGround);
    //set materials
    entGround->setMaterialName("Examples/GrassFloor");
    //set cast shadows
    entGround->setCastShadows(true);

    m_pEnemyLevelMediator = new EnemyLevelMediator(m_entity_manager);

    //Creating the level structure
    Ogre::MeshManager* m_pMeshMgr = Ogre::MeshManager::getSingletonPtr();
    m_pLevel = new Level(m_pSceneMgr, m_pMeshMgr, "BaseWhiteNoLighting", m_entity_manager, m_entity_factory, m_pEnemyLevelMediator);
    m_pLevel->createLevel();

    //initialize the Grid
    m_pLevel->drawQueue = Ogre::RENDER_QUEUE_OVERLAY;
    m_pLevel->attachToNode(m_pSceneMgr->getRootSceneNode());
    m_pLevel->setCellSize(250);
    m_pLevel->ymin = 0;
    m_pLevel->ymax = 0;
    m_pLevel->ofY = 1;
    m_pLevel->zmin = -4;
    m_pLevel->zmax = 4;
    m_pLevel->xmin = -4;
    m_pLevel->xmax = 4;
    m_pLevel->showXZ = true;
    m_pLevel->showXY = false;
    m_pLevel->showYZ = false;
    m_pLevel->setDivisions(4);
    m_pLevel->colorDivision = Ogre::ColourValue(0,1,0,0.3);
    m_pLevel->colorOffset = Ogre::ColourValue(0,0,1,1);
    m_pLevel->colorMaster = Ogre::ColourValue(1,0,0,0.7);

    m_pLevel->update();
    m_pLevel->setTimer();

    m_Player = new Player(m_entity_manager, m_entity_factory, m_pCamera, m_pRSQ, OgreFramework::getSingletonPtr()->m_pKeyboard, OgreFramework::getSingletonPtr()->m_pMouse, m_pSceneMgr, m_pLevel);

    m_collider = new ::Collider(m_entity_manager, m_pEnemyLevelMediator);
    int numEmitters = m_laserParticle->GetParticleSystem()->getNumEmitters();
    OgreFramework::getSingletonPtr()->m_pLog->logMessage("Total Emitters: "+Ogre::StringConverter::toString(numEmitters));
}


bool GameState::keyPressed(const OIS::KeyEvent &keyEventRef)
{
    if(m_bSettingsMode == true)
    {
        if(OgreFramework::getSingletonPtr()->m_pKeyboard->isKeyDown(OIS::KC_S))
        {
            OgreBites::SelectMenu* pMenu = (OgreBites::SelectMenu*)OgreFramework::getSingletonPtr()->m_pTrayMgr->getWidget("DisplayModeSelMenu");
            if(pMenu->getSelectionIndex() + 1 < (int)pMenu->getNumItems())
                pMenu->selectItem(pMenu->getSelectionIndex() + 1);
        }

        if(OgreFramework::getSingletonPtr()->m_pKeyboard->isKeyDown(OIS::KC_W))
        {
            OgreBites::SelectMenu* pMenu = (OgreBites::SelectMenu*)OgreFramework::getSingletonPtr()->m_pTrayMgr->getWidget("DisplayModeSelMenu");
            if(pMenu->getSelectionIndex() - 1 >= 0)
                pMenu->selectItem(pMenu->getSelectionIndex() - 1);
        }
    }

    if(OgreFramework::getSingletonPtr()->m_pKeyboard->isKeyDown(OIS::KC_ESCAPE))
    {
        pushAppState(findByName("PauseState"));
        return true;
    }

    if(OgreFramework::getSingletonPtr()->m_pKeyboard->isKeyDown(OIS::KC_I))
    {
        if(m_pDetailsPanel->getTrayLocation() == OgreBites::TL_NONE)
        {
            OgreFramework::getSingletonPtr()->m_pTrayMgr->moveWidgetToTray(m_pDetailsPanel, OgreBites::TL_TOPLEFT, 0);
            m_pDetailsPanel->show();
        }
        else
        {
            OgreFramework::getSingletonPtr()->m_pTrayMgr->removeWidgetFromTray(m_pDetailsPanel);
            m_pDetailsPanel->hide();
        }
    }

    if(m_bSettingsMode && OgreFramework::getSingletonPtr()->m_pKeyboard->isKeyDown(OIS::KC_RETURN) ||
        OgreFramework::getSingletonPtr()->m_pKeyboard->isKeyDown(OIS::KC_NUMPADENTER))
    {
    }

    if(!m_bSettingsMode || (m_bSettingsMode && !OgreFramework::getSingletonPtr()->m_pKeyboard->isKeyDown(OIS::KC_O)))
        OgreFramework::getSingletonPtr()->keyPressed(keyEventRef);

    return true;
}


bool GameState::keyReleased(const OIS::KeyEvent &keyEventRef)
{
    OgreFramework::getSingletonPtr()->keyPressed(keyEventRef);
    return true;
}


bool GameState::mouseMoved(const OIS::MouseEvent &evt)
{
    if(OgreFramework::getSingletonPtr()->m_pTrayMgr->injectMouseMove(evt)) return true;
    m_Player->mouseMoved(evt);
    return true;
}


bool GameState::mousePressed(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
    if(OgreFramework::getSingletonPtr()->m_pTrayMgr->injectMouseDown(evt, id)) return true;
    m_Player->mousePressed(evt, id);
    return true;
}


bool GameState::mouseReleased(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
    if(OgreFramework::getSingletonPtr()->m_pTrayMgr->injectMouseUp(evt, id)) return true;
    m_Player->mouseReleased(evt, id);
    if(id == OIS::MB_Left)
    {
        m_bLMouseDown = false;
    }
    else if(id == OIS::MB_Right)
    {
        m_bRMouseDown = false;
    }

    return true;
}


void GameState::moveCamera()
{
    SceneNode* cameraNode = m_pSceneMgr->getSceneNode("cameraNode");
    cameraNode->translate(m_TranslateVector);

    //for fixed axis and only xz planes
    //m_pCamera->setFixedYawAxis(true, Vector3::UNIT_Z);
    //m_pCamera->move(m_TranslateVector * 4);
}


void GameState::getInput()
{
    if(m_bSettingsMode == false)
    {
        if(OgreFramework::getSingletonPtr()->m_pKeyboard->isKeyDown(OIS::KC_A))
            //m_TranslateVector.x = -m_MoveScale;
            //for fixed axis and only XZ plane
            //m_TranslateVector.z = -m_MoveScale;
            m_TranslateVector.z = -m_MoveScale;

        if(OgreFramework::getSingletonPtr()->m_pKeyboard->isKeyDown(OIS::KC_D))
            //m_TranslateVector.x = m_MoveScale;
            //for fixed axis and only XZ plane
            m_TranslateVector.z = m_MoveScale;

        if(OgreFramework::getSingletonPtr()->m_pKeyboard->isKeyDown(OIS::KC_W))
            //m_TranslateVector.z = -m_MoveScale;
            //for fixed axis and only XZ plane
            //quat = m_pCamera->getOrientation();
            //m_TranslateVector.z = -m_MoveScale;
            //m_TranslateVector.z = -m_MoveScale;
            m_TranslateVector.x = m_MoveScale;
            //OgreFramework::getSingletonPtr()->m_pLog->logMessage("Moving in direction: "+Ogre::StringConverter::toString(m_TranslateVector));

        if(OgreFramework::getSingletonPtr()->m_pKeyboard->isKeyDown(OIS::KC_S))
            //m_TranslateVector.z = m_MoveScale;
            //for fixed axis and only XZ plane
            m_TranslateVector.x = -m_MoveScale;
    }
}



void GameState::update(double timeSinceLastFrame)
{

    m_FrameEvent.timeSinceLastFrame = timeSinceLastFrame;
    OgreFramework::getSingletonPtr()->m_pTrayMgr->frameRenderingQueued(m_FrameEvent);

    if (m_pIncomeTimer->getMilliseconds() >= 30000){
        m_pIncomeTimer->reset();
        m_Player->AddIncome();
    }

    if (!m_pLevel->GetSpawnComplete()){
        //countdown from 10
        if (m_pCTime->getMilliseconds() >= 1000){
            if (m_seconds > 0){
                m_seconds -= 1;
            }
            m_pCTime->reset();
            if (spawnable){
                m_pLevel->spawnEnemy(Ogre::Vector3(1000,0,-31.25));
            }
        }
        //wait 10 seconds until spawn
        if(m_pLevel->startWave()){
            if (!m_pLevel->GetSpawnComplete()){
                spawnable = true;
            }
            else{
                spawnable = false;
            }
        }
    }
    //reset the timer if spawn complete
    if (m_seconds == 0 && m_pLevel->GetSpawnComplete()){
        //std::cout << "resetting Timer Wave" << "\n";
        m_seconds = 5;
    }

    if (m_pLevel->WaveOver() && m_pLevel->GetSpawnComplete()){
        m_pLevel->ResetSpawnComplete();
        m_pLevel->setTimer();
        m_pLevel->AddWave();

        m_Player->IncreaseIncome();
        spawnable = false;
    }


    if(m_bQuit == true)
    {
        popAppState();
        return;
    }

    if(!OgreFramework::getSingletonPtr()->m_pTrayMgr->isDialogVisible())
    {
        if(m_pDetailsPanel->isVisible())
        {
                m_pDetailsPanel->setParamValue(0, Ogre::StringConverter::toString(Player::m_pLife));
                m_pDetailsPanel->setParamValue(1, Ogre::StringConverter::toString(m_Player->GetIncome()));
                m_pDetailsPanel->setParamValue(2, Ogre::StringConverter::toString(m_Player->GetGold()));

                m_pDetailsPanel->setParamValue(3, Ogre::StringConverter::toString(m_pLevel->GetWave()));
                //m_pDetailsPanel->setParamValue(4, Ogre::StringConverter::toString(m_entity_manager->countEnemies()));
                m_pDetailsPanel->setParamValue(4, Ogre::StringConverter::toString(m_seconds));
                m_pDetailsPanel->setParamValue(5, Ogre::StringConverter::toString(m_Player->GetKillCount()));
        }
    }

    m_MoveScale = m_MoveSpeed   * timeSinceLastFrame;
    m_RotScale  = m_RotateSpeed * timeSinceLastFrame;

    m_TranslateVector = Vector3::ZERO;

    m_collider->Listen(timeSinceLastFrame);
    m_entity_manager->Update(timeSinceLastFrame);
    getInput();
    moveCamera();
    gameOverCheck();
}


void GameState::gameOverCheck(){

    if (Player::m_pLife <= 0){
        //popAllAndPushAppState(findByName("MenuState"));
        recordScore();
        popAllAndPushAppState(findByName("GameOverState"));
    }
}

void GameState::recordScore(){
    std::ofstream scoreFile;
    scoreFile.open("playerscore.txt");
    scoreFile << Player::m_killCount;
    scoreFile.close();


}

void GameState::buildGUI()
{
    OgreFramework::getSingletonPtr()->m_pTrayMgr->showFrameStats(OgreBites::TL_BOTTOMLEFT);
    OgreFramework::getSingletonPtr()->m_pTrayMgr->createLabel(OgreBites::TL_TOP, "GameLbl", "Game mode", 250);
    OgreFramework::getSingletonPtr()->m_pTrayMgr->showCursor();

    Ogre::StringVector items;
    items.push_back("Life");
    items.push_back("Income");
    items.push_back("Gold");
    items.push_back("Wave");
    items.push_back("Next round begins in");
    items.push_back("Enemies defeated");

    m_pDetailsPanel = OgreFramework::getSingletonPtr()->m_pTrayMgr->createParamsPanel(OgreBites::TL_TOPLEFT, "DetailsPanel", 250, items);
    m_pDetailsPanel->show();

    OgreFramework::getSingletonPtr()->m_pTrayMgr->createButton(OgreBites::TL_RIGHT, "Basic_Tower", "Basic Tower", 100);
}


void GameState::buttonHit(OgreBites::Button *button)
{
    if(button->getName() == "Basic_Tower")
    {
        m_Player->buttonHit(button->getName());
    }
}


void GameState::itemSelected(OgreBites::SelectMenu* menu)
{
    switch(menu->getSelectionIndex())
    {
    case 0:
        m_pCamera->setPolygonMode(Ogre::PM_SOLID);break;
    case 1:
        m_pCamera->setPolygonMode(Ogre::PM_WIREFRAME);break;
    case 2:
        m_pCamera->setPolygonMode(Ogre::PM_POINTS);break;
    }
}
