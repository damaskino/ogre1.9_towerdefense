#ifndef ENEMYTOWERMEDIATOR_H
#define ENEMYTOWERMEDIATOR_H

#include "Mediator.h"
class Entity;

class EnemyTowerMediator : public Mediator
{
    public:
        EnemyTowerMediator(EntityManager* entity_manager);
        virtual ~EnemyTowerMediator();
        //void Init();
        void Update(double timeSinceLastFrame);
        void addAndSortByEuclidianDistance(Entity* ent1, Entity* ent2);
        Entity* getNearestEnemy(Entity* towerEntity);
    protected:
    private:
         EntityManager* m_entity_manager;
};

#endif // ENEMYTOWERMEDIATOR_H
