#include "EntityManager.h"
#include "Entity.h"
#include "Player.h"
#include "Enemy.h"

EntityManager::EntityManager(Ogre::SceneManager* scene_manager, Ogre::Log* logger)
	: m_scene_manager(scene_manager), m_logger(logger){
	    borderInserted = false;
	    //initialize vector with GridWidth in Cells
        spatialGrid.resize(32);
	}


EntityManager::~EntityManager(void){
	if (m_entities.empty())
		return;

	auto it = m_entities.begin();

	while (it != m_entities.end()){
		m_scene_manager->destroySceneNode((*it)->GetSceneObject());
		delete *it;
		*it = NULL;
		it++;
	}
	m_entities.clear();
}

void EntityManager::AddEntity(Entity* entity){
	m_entities.push_back(entity);
	//m_logger->logMessage("Created Entity with id: " + Ogre::StringConverter::toString(entity->GetId()));
	//m_logger->logMessage("Total numbers of Entities: " + Ogre::StringConverter::toString(m_entities.size()));
}

size_t EntityManager::GetEntities(){
    return m_entities.size();
}

void EntityManager::RemoveEntity(Entity* entity){
	m_queue_list.push_back(entity);
}

void EntityManager::Update(float deltatime) {
	if (!m_queue_list.empty()) {
		for (auto it = m_queue_list.begin(); it != m_queue_list.end(); it++){
			Remove(*it);
		}
		m_queue_list.clear();
	}
    //Update each entity
	for (unsigned int i = 0; i < m_entities.size(); i++){
		m_entities[i]->Update(deltatime);
	}
}

// remapping world coordinate 0/0/0 to the lower left corner, left-most cell at the bottom is 0/0
//unit is 62.5
std::tuple<int, int> EntityManager::posInGrid(double x, double z, double unit){
    double xFromCenter = x;
    double zFromCenter = z;
    int col = std::floor((zFromCenter-(-1032.5))/unit);
    int row = std::floor((xFromCenter-(-1032.5))/unit);
    return std::tuple<int, int>(col-1, row-1);
}


std::vector<std::vector<std::vector<Entity*>>> EntityManager::UpdateSpatialGrid(){

    spatialGrid.clear();
    spatialGrid.resize(32);

    if (m_entities.empty()){
        return spatialGrid;
    }
    else{

        for (unsigned int i = 0; i < m_entities.size(); i++){
            Ogre::Vector3 vPos = m_entities[i]->GetPosition();
            //m_logger->logMessage("EntityType : "+std::to_string(m_entities[i]->GetType()));
            EEntityType entType = m_entities[i]->GetType();
            if ( (entType == Bullet_Type) || (entType == Particle_Type) ){
                continue;
            }

            if ( (entType == Border_Type) && (!borderInserted)){
                std::cout << "inserted Border to spatial Grid" << std::endl;
                std::cout << "\n" << std::endl;
                for (unsigned int col = 20; col < 28; col++){
                    if (spatialGrid[col].empty()){
                        spatialGrid[col].resize(32);
                    }
                    for(unsigned int row = 4; row < 12; row++){
                        if (spatialGrid[col][row].empty()){
                            spatialGrid[col][row] = std::vector<Entity*>();
                        }
                        spatialGrid[col][row].push_back(m_entities[i]);
                    }
                }

                for (unsigned int col = 20; col < 28; col++){
                    if (spatialGrid[col].empty()){
                        spatialGrid[col].resize(32);
                    }
                    for(unsigned int row = 20; row < 28; row++){
                        if (spatialGrid[col][row].empty()){
                            spatialGrid[col][row] = std::vector<Entity*>();
                        }
                        spatialGrid[col][row].push_back(m_entities[i]);
                    }
                }

                for (unsigned int col = 4 ; col < 12; col++){
                    if (spatialGrid[col].empty()){
                        spatialGrid[col].resize(32);
                    }
                    for(unsigned int row = 4; row < 12; row++){
                        if (spatialGrid[col][row].empty()){
                            spatialGrid[col][row] = std::vector<Entity*>();
                        }
                        spatialGrid[col][row].push_back(m_entities[i]);
                    }
                }

                for (unsigned int col = 4; col < 12; col++){
                    if (spatialGrid[col].empty()){
                        spatialGrid[col].resize(32);
                    }
                    for(unsigned int row = 20; row < 28; row++){
                        if (spatialGrid[col][row].empty()){
                            spatialGrid[col][row] = std::vector<Entity*>();
                        }
                        spatialGrid[col][row].push_back(m_entities[i]);
                    }
                }
                borderInserted = true;
            }

            else if (entType == Basic_Tower){

                Ogre::Vector3 towerPos = m_entities[i]->GetPosition();
                std::tuple<int,int> towerGridPos = posInGrid(towerPos[0], towerPos[2], 62.5);
                int center_col = std::get<0>(towerGridPos);
                int center_row = std::get<1>(towerGridPos);

                //reset tower indices if we're getting out of grid range
                int col;
                int row;

                if (center_col-2 < 0){
                    col = 0;
                }
                else if (center_col >= 30){
                    center_col = 29;
                    col = 29;
                }
                else{
                    col = center_col-2;
                }

                if (center_row-2 < 0){
                    row = 0;
                }
                else if (center_row >= 30){
                    center_row = 29;
                    row = 29;
                }
                else{
                    row = center_row-2;
                }


                for( int currentcol = col; currentcol<center_col+3; currentcol++){
                    if (spatialGrid[currentcol].empty()){
                        spatialGrid[currentcol].resize(32);
                    }

                    for( int currentrow = row; currentrow<center_row+3; currentrow++){
                        if (spatialGrid[currentcol][currentrow].empty()){
                            spatialGrid[currentcol][currentrow] = std::vector<Entity*>();
                        }
                        spatialGrid[currentcol][currentrow].push_back(m_entities[i]);
                    }
                }
            }

            else{

                if (entType == Border_Type){
                    continue;
                }
                std::tuple<int, int> t = this->posInGrid(vPos[0], vPos[2], 62.5);
                int col = std::get<0>(t);
                if (spatialGrid[col].empty()){
                    spatialGrid[col].resize(32);
                }
                int row = std::get<1>(t);
                if (spatialGrid[col][row].empty()){
                    spatialGrid[col][row] = std::vector<Entity*>();
                }
                spatialGrid[col][row].push_back(m_entities[i]);
            }
        }
    }
    return spatialGrid;
}


std::vector<std::vector<std::vector<Entity*>>> EntityManager::GetSpatialGrid(){
    return this->spatialGrid;
}


inline void EntityManager::Remove(Entity* entity){
	for (auto it = m_entities.begin(); it != m_entities.end(); ){
		if (*it == entity){
            if (entity->GetType() == Enemy_Type){
                std::cout << "enemy life at deletion: " << ((Enemy*)entity)->GetLife() << std::endl;
                if (((Enemy*)entity)->GetLife() <= 0){
                    Player::m_pGold += 2;
                }
            }
			m_scene_manager->destroySceneNode((*it)->GetSceneObject());
			it = m_entities.erase(it);
			m_logger->logMessage("Deleted Entity with Id: " + Ogre::StringConverter::toString(entity->GetId()) + " Of Type: " +  Ogre::StringConverter::toString(entity->GetType()));
			m_logger->logMessage("Total numbers of Entities after deletion: " + Ogre::StringConverter::toString(m_entities.size()));
			entity = NULL;
			delete entity;
			return;
		}
		else{
            ++it;
		}
	}
}


Entity* EntityManager::GetEntity(unsigned int position){
    Entity* ent = m_entities.at(position);
    return ent;
}


int EntityManager::countEnemies(){
    int nEnemies = 0;
    for (auto it = m_entities.begin(); it != m_entities.end(); it++){
		if ((*it)->GetType() == Enemy_Type){
            nEnemies ++;
		}
    }
    return nEnemies;
}


Entity* EntityManager::GetLastBuiltTower(){
    for (auto it = m_entities.rbegin(); it != m_entities.rend(); ++it){
		if ((*it)->GetType() == Basic_Tower){
            return (*it);
		}
    }
}


Entity* EntityManager::GetFirstEnemy(){
    for (auto it = m_entities.begin(); it != m_entities.end(); it++){
		if ((*it)->GetType() == Enemy_Type){
            return (*it);
		}
    }
}


std::vector<Entity*> EntityManager::GetAllEnemies(){

    std::vector<Entity*> enemies;
    for (auto it = m_entities.begin(); it!= m_entities.end(); it++ ){
        std::cout << "Entity type is: " << (*it)->GetType() << std::endl;
        if ((*it)->GetType() == Enemy_Type){
            enemies.push_back(*it);

        }
    }
    return enemies;
}


void EntityManager::printSpatialGrid(){



}
