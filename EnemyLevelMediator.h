#ifndef ENEMYLEVELMEDIATOR_H
#define ENEMYLEVELMEDIATOR_H

#include "Mediator.h"
#include "Pathplanning.h"
#include "Level.h"


class Entity;


class EnemyLevelMediator : public Mediator
{


    std::vector<Ogre::Vector3> m_currentPath;

    public:
        EnemyLevelMediator(EntityManager* entity_manager);
        virtual ~EnemyLevelMediator();

        void Update(double timeSinceLastFrame);
        void updateEnemyPaths(std::vector<Ogre::Vector3> path);

        void planGeneralPath();
        std::vector<Ogre::Vector3> planSinglePath(int, int);
        std::vector<Ogre::Vector3> InitialEnemyPath();

        void setCurrentPath(std::vector<Ogre::Vector3> path);
        std::vector<Ogre::Vector3> getCurrentPath();

        void initPathplanning();

        float m_GridArray[32][32];

        //Debugging functions
        void printGrid();
        void printCurrentPath();


    protected:
    private:
         EntityManager* m_entity_manager;
         Pathplanning* m_pathplanner;

};

#endif // ENEMYLEVELMEDIATOR_H

