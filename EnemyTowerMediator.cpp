#include "EnemyTowerMediator.h"
#include "BasicTower.h"
#include "Enemy.h"

class Entity;

struct DistanceStruct{
    Ogre::Real distance;
    Entity* enemy;
    DistanceStruct(Ogre::Real distance, Entity* enemy) : distance(distance), enemy(enemy) {}
};

struct less_than_key
{
    inline bool operator() (const DistanceStruct& struct1, const DistanceStruct& struct2)
    {
        return (struct1.distance < struct2.distance);
    }
};

EnemyTowerMediator::EnemyTowerMediator(EntityManager* entity_manager) : Mediator(entity_manager)
{
    m_entity_manager = entity_manager;
}

EnemyTowerMediator::~EnemyTowerMediator()
{
    m_entity_manager->~EntityManager();
}


void EnemyTowerMediator::Update(double timeSinceLastFrame){
    m_entity_manager->Update(timeSinceLastFrame);
}

void EnemyTowerMediator::addAndSortByEuclidianDistance(Entity* ent1, Entity* ent2){
    //add collided Entity to tower range
    ((BasicTower*)ent2)->nearestEnemies.push_back(ent1);
    std::vector<Entity*> nE = ((BasicTower*)ent2)->nearestEnemies;
    //sort enemies in reach according to tower distance
    //perform sorting in temp vector
    std::vector<DistanceStruct> tmp;
    for (auto it = nE.begin(); it!= nE.end(); it++){
        Ogre::Vector3 src = ent2->GetSceneObject()->getOrientation() * Ogre::Vector3::UNIT_X;
        Ogre::Vector3 destination = ent1->GetSceneObject()->_getWorldAABB().getCenter();
        Ogre::Vector3 direction = destination - ent2->GetSceneObject()->getPosition();
        src.y = 0;
        direction.y = 0;

        Ogre::Real distance = direction.normalise();

        //Ignore enemies that have gone out of range.
        if (distance > ((BasicTower*)ent2)->m_radius){
            continue;
        }
        tmp.push_back(DistanceStruct(distance, (*it)));
    }

    std::sort(tmp.begin(), tmp.end(), less_than_key());
    //replace tower's old enemy vector
    ((BasicTower*)ent2)->nearestEnemies.clear();
    for (auto it = tmp.begin(); it != tmp.end(); it++){
        Entity* ent = (*it).enemy;
        ((BasicTower*)ent2)->nearestEnemies.push_back(ent);
    }
    tmp.clear();
    nE.clear();
}

Entity* EnemyTowerMediator::getNearestEnemy(Entity* towerEntity){
    Entity* nNEnemy = ((BasicTower*)towerEntity)->nearestEnemies.front();
    return nNEnemy;
}
