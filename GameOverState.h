

#ifndef GAME_OVER_STATE_H
#define GAME_OVER_STATE_H



#include "AppState.h"



class GameOverState : public AppState
{
public:
    GameOverState();

    DECLARE_APPSTATE_CLASS(GameOverState)

    void enter();
    void createScene();
    void exit();

    bool keyPressed(const OIS::KeyEvent &keyEventRef);
    bool keyReleased(const OIS::KeyEvent &keyEventRef);

    bool mouseMoved(const OIS::MouseEvent &evt);
    bool mousePressed(const OIS::MouseEvent &evt, OIS::MouseButtonID id);
    bool mouseReleased(const OIS::MouseEvent &evt, OIS::MouseButtonID id);

    void buttonHit(OgreBites::Button* button);
    void yesNoDialogClosed(const Ogre::DisplayString& question, bool yesHit);

    void update(double timeSinceLastFrame);

    int getPlayerScore();

private:
    bool                        m_bQuit;
    bool                        m_bQuestionActive;
};



#endif


