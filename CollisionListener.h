#ifndef COLLISIONLISTENER_H
#define COLLISIONLISTENER_H


class CollisionListener
{
    public:
        CollisionListener(){
        }
        virtual ~CollisionListener(){}
        //Init: instantiate pathplanning and collider
        virtual void Listen(double timeSinceLastFrame)=0;

    protected:
    private:


};

#endif // CollisionListener_H

