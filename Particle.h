#ifndef PARTICLE_H
#define PARTICLE_H

#include "Entity.h"
#include "ParticleSystem.h"

class EntityManager;
class EntityFactory;
class Particle : public Entity
{
    public:
        /** Default constructor */
        Particle(const EEntityType& entity_type, EntityFactory* entity_factory, EntityManager* entity_manager);
        /** Default destructor */
        virtual ~Particle(void);
        void Init(Ogre::SceneManager* scene_manager);
        void SetPosition(Ogre::Vector3 pos);
        void AddEmitter(MyParticleSystem* m_PS);
        void Update(double deltatime);
        typedef Entity* Base;
    protected:
        EntityFactory* m_entity_factory;
        EntityManager* m_entity_manager;
        Ogre::ParticleEmitter* emitter;
    private:
};

#endif // PARTICLE_H
