#include "ParticleSystem.h"

MyParticleSystem::MyParticleSystem(Ogre::SceneManager* scene_manager, Ogre::Log* logger): m_scene_manager(scene_manager), m_logger(logger)
{
    //ctor
    m_particle_system = m_scene_manager->createParticleSystem("Laser", "Example/Laser");
    m_logger->logMessage("ParticleSystem created");
    m_particle_system->getEmitter(0)->setEnabled(false);
    Ogre::SceneNode* particle = m_scene_manager->getRootSceneNode()->createChildSceneNode("ParticleSystemTowers");
    particle->attachObject(m_particle_system);
}

MyParticleSystem::~MyParticleSystem()
{
    //dtor
    m_particle_system->clear();
}

Ogre::ParticleSystem* MyParticleSystem::GetParticleSystem(){
    return this->m_particle_system;
}
