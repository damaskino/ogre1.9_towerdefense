#ifndef ENTITYMANAGER_H
#define ENTITYMANAGER_H

#include "Entity.h"
#include <OgreLogManager.h>
#include <tuple>
#include <vector>

class EntityManager : public EntityListener
{
public:
	EntityManager(Ogre::SceneManager* scene_manager, Ogre::Log* logger);
	~EntityManager(void);

	Entity* GetEntity(unsigned int position);
	Entity* GetEntity(Entity* entity);
	Entity* GetLastBuiltTower();
	Entity* GetFirstEnemy();
	std::vector<Entity*> GetAllEnemies();
	size_t GetEntities();

	void AddEntity(Entity* entity);
    int countEnemies();

	void RemoveEntity(Entity* entity);
	//void RemoveEntity(unsigned int id);

	void Update(float deltatime);
	void printSpatialGrid();
	std::vector<std::vector<std::vector<Entity*>>> UpdateSpatialGrid();
	std::vector<std::vector<std::vector<Entity*>>> GetSpatialGrid();
    std::tuple<int, int> posInGrid(double x, double z, double length);

private:
    bool borderInserted;
	inline void Remove(Entity* entity);
	std::vector<Entity*> m_entities;
	std::vector<Entity*> m_queue_list;
	Ogre::SceneManager* m_scene_manager;
	Ogre::Log* m_logger;
    std::vector<std::vector<std::vector<Entity*>>> spatialGrid;
};

#endif //ENTITYMANAGER_H
