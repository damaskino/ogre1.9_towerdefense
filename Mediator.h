#ifndef MEDIATOR_H
#define MEDIATOR_H

#include "EntityManager.h"

class Mediator
{
    public:
        Mediator(EntityManager* entity_manager){
        }
        virtual ~Mediator(){}
        //virtual void Init()=0;
        //Init: instantiate pathplanning and collider
        virtual void Update(double timeSinceLastFrame)=0;
        //Update when colliding (subtract life, set position)
        //may need to rewrite enemy function

    protected:
    private:


};

#endif // MEDIATOR_H
