#ifndef BUILDER_H
#define BUILDER_H

#include "OgreMeshManager.h"
#include "OgreSceneManager.h"
#include "Entity.h"


class Builder
{
    public:
        /** Default constructor */
        Builder() {}
        /** Default destructor */
        virtual ~Builder() {}
        virtual void BuildPart(Ogre::SceneManager* scene_manager, Ogre::MeshManager* mesh_mgr, int degree, int height, int width, int wallHeight, Ogre::Vector3 vec, Entity* ent) = 0;
    protected:
    private:
};

#endif // BUILDER_H
