#ifndef BORDERBUILDER_H
#define BORDERBUILDER_H

#include "Builder.h"

class BorderBuilder : public Builder
{
    public:
        /** Default constructor */
        BorderBuilder();
        /** Default destructor */
        virtual ~BorderBuilder();
        void BuildPart(Ogre::SceneManager* scene_manager, Ogre::MeshManager* mesh_mgr, int degree, int height, int width, int wallHeight, Ogre::Vector3 vec, Entity* ent);

    protected:
    private:
};

#endif // BORDERBUILDER_H
