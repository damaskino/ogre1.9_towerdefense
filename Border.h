#ifndef BORDER_H
#define BORDER_H

#include "Entity.h"

class EntityManager;
class EntityFactory;
class Border : public Entity
{
    public:
        /** Default constructor */
        Border(const EEntityType& entity_type, EntityFactory* entity_factory, EntityManager* entity_manager);
        /** Default destructor */
        virtual ~Border(void);

        void Init(Ogre::SceneManager* scene_manager);
        void Update(double deltatime);
        void SetPosition(Ogre::Vector3 pos);

        int degree;
        std::string resource_name;

    protected:
        EntityFactory* m_entity_factory;
        EntityManager* m_entity_manager;
    private:
};

#endif // BORDER_H
