#include "Particle.h"
#include "EntityFactory.h"
#include "EntityManager.h"

Particle::Particle(const EEntityType& entity_type, EntityFactory* entity_factory, EntityManager* entity_manager) : Entity(entity_type){
    //ctor
	m_entity_factory = entity_factory;
	m_entity_manager = entity_manager;
}

Particle::~Particle(void)
{
    //dtor
}


void Particle::Init(Ogre::SceneManager* scene_manager){
    //std::string resource_name = "Particle " + std::to_string(this->GetId());
    //Ogre::Entity* OgreEnt = scene_manager->createEntity(std::to_string(this->GetId()), resource_name);
    m_scene_object = scene_manager->getRootSceneNode()->createChildSceneNode(std::to_string(this->GetId()));
    m_scene_object->setPosition(this->m_position);
    this->emitter->setEnabled(true);
	this->emitter->setPosition(this->m_position);

    /* DEPRECATED
    //TODO later GetEntityByID will work here as soon as the mediator knows which Towers are in range and have to shoot so that code is to add here or another place
    Entity* towerEntity = m_entity_manager->GetLastBuiltTower();
    //TODO later we want to add the particle effects to the projectiles instead the Tower
    this->m_scene_object = towerEntity->GetSceneObject();
    Ogre::LogManager::getSingletonPtr()->logMessage("attach particlesystem on tower with id: " + std::to_string(towerEntity->GetId()));
    this->m_position = m_scene_object->getPosition();
    this->emitter->setEnabled(true);
	this->emitter->setPosition(this->m_position);
	*/
}

void Particle::SetPosition(Ogre::Vector3 pos){
    m_position = pos;
}

void Particle::Update(double deltatime){
}

void Particle::AddEmitter(MyParticleSystem* m_PS){
    Ogre::ParticleEmitter* genericEmitter = m_PS->GetParticleSystem()->getEmitter(0);
    this->emitter = m_PS->GetParticleSystem()->addEmitter("Point");
    genericEmitter->copyParametersTo(emitter);

    //things we can also do here or somwhere different
    //m_PS->GetParticleSystem()->setParticleQuota(m_PS->GetParticleSystem()->getParticleQuota() + m_HitParticleOriginalQuota);
    /*this->emitter->setAngle(Ogre::Radian(0));
    this->emitter->setEmissionRate(1000);
    this->emitter->setDirection(Ogre::Vector3(0.0, 1.0, 0.0));
    this->emitter->setColour(Ogre::ColourValue(1.0, 0.0, 0.0, 1.0));*/
    //this->emitter->setEnabled(false);
}
