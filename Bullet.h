#ifndef BULLET_H
#define BULLET_H

#include "Entity.h"

class EntityManager;
class EntityFactory;
class Bullet : public Entity
{
    public:
        /** Default constructor */
        Bullet(const EEntityType& entity_type, EntityFactory* entity_factory, EntityManager* entity_manager);
        /** Default destructor */
        virtual ~Bullet();
        void Init(Ogre::SceneManager* scene_manager);
        void Update(double deltatime);
        void SetPosition(Ogre::Vector3 pos);
        void SetSource(Ogre::Vector3 position);
        void SetDestination(Ogre::Vector3 destination);
        void PredictIntersect(Ogre::Vector3 enemyVelocity);//, Ogre::Vector3 bulletVelocity);
        void SetEnemy(Entity* enemy);
        void SetTower(Entity* tower);
    protected:
    private:
        EntityFactory* m_entity_factory;
        EntityManager* m_entity_manager;
        Ogre::Vector3 m_towerPos;
        Ogre::Vector3 m_enemyPos;
        Ogre::Real speed;
        Ogre::Real m_distance;
        Ogre::Vector3 m_aimSpot;
        Ogre::Vector3 m_bulletPath;
        float m_timeToImpact;
        Entity* m_enemyToHit;
        Entity* m_tower;
};

#endif // BULLET_H
