

#ifndef GAME_STATE_H
#define GAME_STATE_H



#include "AppState.h"

#include <OgreSubEntity.h>
#include <OgreMaterialManager.h>
#include <sstream>
#include <OgreQuaternion.h>
#include <OgreTimer.h>
#include "Player.h"
#include "Level.h"


enum QueryFlags
{
    GROUND_ENTITY = 1 << 0,
    BORDER_ENTITY = 1 << 1,
    GRID_ENTITY = 1 << 2
};

class EntityManager;
class EntityFactory;
class MyParticleSystem;
class Player;
class Level;
class Collider;


class GameState : public AppState
{
public:
	GameState();

	DECLARE_APPSTATE_CLASS(GameState)

	void enter();
	void createScene();
	void exit();
	bool pause();
	void resume();

	void moveCamera();
	void getInput();
    void buildGUI();

	bool keyPressed(const OIS::KeyEvent &keyEventRef);
	bool keyReleased(const OIS::KeyEvent &keyEventRef);

	bool mouseMoved(const OIS::MouseEvent &arg);
	bool mousePressed(const OIS::MouseEvent &arg, OIS::MouseButtonID id);
	bool mouseReleased(const OIS::MouseEvent &arg, OIS::MouseButtonID id);

	void onLeftPressed(const OIS::MouseEvent &evt);
    void itemSelected(OgreBites::SelectMenu* menu);

    void recordScore();

	void update(double timeSinceLastFrame);
	void buttonHit(OgreBites::Button* button);
	void gameOverCheck();

private:
    Ogre::SceneNode*            cubes;
    OgreBites::ParamsPanel*		m_pDetailsPanel;
	bool						m_bQuit;

	Ogre::Vector3				m_TranslateVector;
	Ogre::Real					m_MoveSpeed;
	Ogre::Degree				m_RotateSpeed;
	float						m_MoveScale;
	Ogre::Degree				m_RotScale;

	Ogre::RaySceneQuery*		m_pRSQ;
	Ogre::SceneNode*			m_pCurrentObject;
    EntityFactory*              m_entity_factory;
	EntityManager*              m_entity_manager;
	MyParticleSystem*           m_laserParticle;
	Player*                     m_Player;
	bool						m_bLMouseDown, m_bRMouseDown;
	bool						m_bSettingsMode;
	int                         m_numberOfTowers;
	EnemyLevelMediator*         m_pEnemyLevelMediator;
	Level*                      m_pLevel;
    Ogre::Quaternion            quat;
    int                         m_seconds;
    Ogre::Timer*                m_pCTime;
    Ogre::Timer*                m_pIncomeTimer;
    bool                        spawnable;
    Collider*                   m_collider;
};



#endif


