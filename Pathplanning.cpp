#include "Pathplanning.h"
#include <iostream>
#include <cmath>
#include <vector>
#include <algorithm>


Pathplanning::Pathplanning()
{
    const int colMaximum = 32;
    const int rowMaximum = 32;
    node m_NodeArray[colMaximum][rowMaximum];

    //ctor
}

Pathplanning::~Pathplanning()
{
    //dtor
}


void Pathplanning::findPath(int startCol, int startRow){

    node start = m_NodeArray[startCol][startRow];
    node* goal = &m_NodeArray[15][0];

    m_open.push(start);


    while(goal->id != m_open.top().id){

        node q = m_open.top();
        m_open.pop();
        printNodeGrid();
        std::cout << "Checking node with id: " << q.id << std::endl;
        std::vector<node> neighbours = checkNeighbours(&q);
        std::cout << "Done checking neighbours" << std::endl;

        //neighbours.push_back(goal);
        for(int i=0; i < neighbours.size(); i++){

            neighbours[i].g = q.g + distance(neighbours[i], q);
            neighbours[i].h = distance(*goal, neighbours[i]);
            neighbours[i].f = neighbours[i].g + neighbours[i].h;

            int currentCol = neighbours[i].col;
            int currentRow = neighbours[i].row;

            m_NodeArray[currentCol][currentRow].g = q.g + distance(neighbours[i], q);
            m_NodeArray[currentCol][currentRow].h = distance(*goal, neighbours[i]);


            //Search the open list
            if (findInQueue(m_open, neighbours[i]))
            {
                std::cout << "FOUND IT IN THE OPEN LIST" << std::endl;
                std::cout << neighbours[i].f << std::endl;
                std::cout << m_NodeArray[currentCol][currentRow].f << std::endl;
                if(neighbours[i].f < m_NodeArray[currentCol][currentRow].f){
                    removeFromQueue(m_open, neighbours[i]);
                    continue;
                }
            }

            //Search the closed list
            if (m_closed.find(neighbours[i]) != m_closed.end()){
                if(neighbours[i].f < m_NodeArray[currentCol][currentRow].f){
                    continue;
                }
            }

            if (!(m_closed.find(neighbours[i]) != m_closed.end()) and !(findInQueue(m_open, neighbours[i]))){
                m_NodeArray[currentCol][currentRow].parent = &q;
                m_NodeArray[currentCol][currentRow].parentCol = q.col;
                m_NodeArray[currentCol][currentRow].parentRow = q.row;
                std::cout << "Parent for " << m_NodeArray[currentCol][currentRow].id << " set to ";
                std::cout << m_NodeArray[currentCol][currentRow].parent->id << std::endl;

                m_open.push(neighbours[i]);
                m_NodeArray[neighbours[i].col][neighbours[i].row].f = neighbours[i].f;

            }
        }

       m_closed.insert(q);
    }

    std::cout << "Finished Path finding, tracing the path...." << std::endl;
    m_calculatedPath = tracePath(*goal, start);



    clearQueue();
    m_closed.clear();

    printGrid();
}



void Pathplanning::update(){

}


void Pathplanning::init(){

    int m_idCount = 0;

    //Build NodeArray from GridArray
    for(int col=0; col < 32; col++){
        std::cout << std::endl;
        for(int row=0; row < 32; row++){
            node myNode = node();
            myNode.id = m_idCount;
            myNode.col = col;
            myNode.row = row;
            myNode.f = m_GridArray[col][row];
            myNode.g = 0.0;
            myNode.h = 0.0;

            myNode.parent = NULL;
            myNode.parentCol = 0;
            myNode.parentRow = 0;
            m_idCount++;
            m_NodeArray[col][row] = myNode;
        }
    }


}


//The heuristic for determining a distance between two nodes
float Pathplanning::distance(node start, node finish){
    //heuristics goes here
    float result;
    //This is for diagonal movement only
    //result = pow(start.col - finish.col, 2) + pow(start.row - finish.row, 2);

    result = abs(start.col - finish.col) + abs(start.row - finish.row);

    return result;
}


std::vector<node> Pathplanning::checkNeighbours(node* current){

    std::cout << "Looking at Node: " << current->id << std::endl;

    std::vector<node> neighbours;
    for(int i = current->col-1; i<= current->col+1; i++){

            //Ignore a neighbour if it would be out of bounds or if it is blocked
            if (i == current->col){ continue; }
            if (m_NodeArray[i][current->row].f == INFINITY){ continue; }
            if (i>=colMaximum){ continue; }
            if (i<0){ continue; }

            neighbours.push_back(m_NodeArray[i][current->row]);

    }

    for(int i= current->row-1; i<= current->row+1; i++){

            //Ignore a neighbour if it would be out of bounds or if it is blocked
            if (i == current->row){ continue; }
            if (m_NodeArray[current->col][i].f == INFINITY){ continue; }
            if (i>=rowMaximum){ continue; }
            if (i<0){ continue; }

            neighbours.push_back(m_NodeArray[current->col][i]);
    }
    return neighbours;
}


bool Pathplanning::findInQueue(NodeQueue pQueue, node curNode){

   NodeQueue tmpQueue = pQueue;
    while (!tmpQueue.empty()){
        node checkNode = tmpQueue.top();
        if (checkNode.id == curNode.id){
            return true;
        }
        tmpQueue.pop();
    }
    return false;

}


std::vector<Ogre::Vector3> Pathplanning::tracePath(node child, node ancestor){

    std::vector<Ogre::Vector3> newPath;
    std::cout << "Tracing the path: " << std::endl;
    node tmp;
    tmp = child;
    while(tmp.id != ancestor.id){

        if (tmp.id==0){

            exit(0);
        }

        float worldX = convertToWorldCoord(tmp.row);
        float worldZ = convertToWorldCoord(tmp.col);
        newPath.push_back(Ogre::Vector3(worldX, 0 , worldZ));
        tmp = m_NodeArray[tmp.parentCol][tmp.parentRow];
    }

    float goalX = convertToWorldCoord(ancestor.row);
    float goalZ = convertToWorldCoord(ancestor.col);
    newPath.push_back(Ogre::Vector3(goalX, 0, goalZ));

    for(auto it = newPath.begin(); it != newPath.end(); it++){
        std::cout << "New path X: "<< (*it).x;
        std::cout << ", New path Z: " << (*it).z << std::endl;
    }

    printNodeGrid();
    return newPath;
}

std::vector<Ogre::Vector3> Pathplanning::getPath(){

    return m_calculatedPath;
}


//Convert Array indices to world coordinates
float Pathplanning::convertToWorldCoord(int coord){

    float worldCoord = -1000 + (2 * coord * 31.25) + 31.25;
    return worldCoord;
}


NodeQueue Pathplanning::removeFromQueue(NodeQueue pQueue, node curNode){

    NodeQueue tmpQueue = pQueue;
    NodeQueue resultQueue;
    //copy the queue, leaving out the one element we want to delete
    while (!tmpQueue.empty()){
        node checkNode = tmpQueue.top();
        if (checkNode.id == curNode.id){
            tmpQueue.pop();
            continue;
        }
        resultQueue.push(checkNode);
        tmpQueue.pop();

    }
    return resultQueue;

}


void Pathplanning::clearQueue(){

    NodeQueue emptyQueue;
    std::swap(m_open, emptyQueue);

}


//For Debugging only
void Pathplanning::printNodeGrid(){

    for(int row=31; row >= 0; row--){
        std::cout << std::endl;
        for(int col=0; col < 32; col++){

            //if (m_NodeArray[col][row].f == 0.0){
                std::cout << "  ";
            //}
            std::cout << m_NodeArray[col][row].h;
        }
    }
    std::cout << std::endl;
}


//For Debugging only
void Pathplanning::printGrid(){

    for(int row=31; row >= 0; row--){
        std::cout << std::endl;
        for(int col=0; col < 32; col++){
            if (m_GridArray[col][row] == 0){
                std::cout << "  ";
            }
            std::cout << m_GridArray[col][row];
        }
    }
    std::cout << std::endl;
}
