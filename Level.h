#ifndef LEVEL_H
#define LEVEL_H

#include "OgreMeshManager.h"
#include "OgreSceneManager.h"
#include "OgreEntity.h"
#include "Grid.h"
#include <tuple>
#include "EnemyLevelMediator.h"
#include "EntityManager.h"
#include "EntityFactory.h"


class EnemyLevelMediator;


class Level : public Grid{

public:
        Level(Ogre::SceneManager* smPtr, Ogre::MeshManager* mmPtr, Ogre::String material, EntityManager* m_entity_manager, EntityFactory* m_entity_factory, EnemyLevelMediator* m_pEnemyLevelMediator);

        //void buildPlane(int, int, Ogre::Vector3, Ogre::Vector3, Ogre::Entity*);
        void buildPlane(int, int, int, Ogre::Vector3, Ogre::Vector3, int);
        void buildBlock(int, int, Ogre::Vector3);
        void createLevel();
        // These setters are for convenience only, you MUST call update() after use!
        inline void setCellSize(float size){csX=csY=csZ=size;}
        inline void setCellSize(float x, float y, float z){csX=x;csY=y,csZ=z;}
        inline void setOffset(float x, float y, float z){ofX=x,ofY=y,ofZ=z;}
        inline void setDivisions(int n){divX=divY=divZ=n;}
        inline void setDivisions(int x,int y,int z){divX=x;divY=y;divZ=z;}
        inline void showPlanes(bool xy, bool xz, bool yz){showXY=xy;showXZ=xz;showYZ=yz;}
        inline Ogre::Vector3 getDivisions(){return Ogre::Vector3(divX, divY, divZ);}

        //Attach the grid to your provided sceneNode.
        void attachToNode(Ogre::SceneNode* node);

        std::tuple<int, int> posInGridCell(double x, double z, double length);
        std::tuple<int, int> posInGrid(double x, double z, double unit);

        //After setup, call this to regenerate the Grid.
        void update();

        //Grid's visibility
        void hide();
        void show();

        const static int hCells = 32;
        const static int vCells = 32;
        float m_GridArray[hCells][vCells];
        void fillGridBlock(int, int, int, int);
        void printGrid();
        void updateMediator();
        float* getGridArray();

        //Related to enemy spawning
        void AddWave();
        int GetWave();
        bool startWave();
        void setTimer();
        void spawnEnemy(Ogre::Vector3 pos);
        bool GetSpawnComplete();
        bool buttonHit(const Ogre::String& name);
        void ResetSpawnComplete();
        bool WaveOver();
        bool goalBlocked(int, int);


private:
        Ogre::MeshManager* m_pMeshMgr;
        Ogre::SceneManager* m_pSceneMgr;
        EntityManager* m_pEntityManager;
        EntityFactory* m_pEntityFactory;
        EnemyLevelMediator* m_pEnemyLevelMediator;

        int                         m_Wave;
        int                         m_Income;
        int                         m_enemySeconds;
        bool                        m_spawnComplete;

        Ogre::Timer*                m_pWaveTimer;
        Ogre::Timer*                m_pEnemyTimer;

};



#endif // LEVEL_H
