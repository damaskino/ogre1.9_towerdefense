#include "EnemyLevelMediator.h"
#include "Enemy.h"
#include "OgreVector3.h"
#include <typeinfo>
#include <cmath>



class Entity;

EnemyLevelMediator::EnemyLevelMediator(EntityManager* entity_manager) : Mediator(entity_manager)
{
    //ctor
    m_entity_manager = entity_manager;
    m_pathplanner = new Pathplanning();

    //m_currentPath is the current best path from the start to the goal and will be assigned to all enemies that spawn
    m_currentPath = InitialEnemyPath();

}

EnemyLevelMediator::~EnemyLevelMediator()
{
    //dtor
    m_entity_manager->~EntityManager();
}

void EnemyLevelMediator::Update(double timeSinceLastFrame){
}


void EnemyLevelMediator::initPathplanning(){


   for (int i = 0; i<32; i++){
            for (int j = 0; j<32; j++){
                   m_pathplanner->m_GridArray[i][j] = m_GridArray[i][j];
            }

    }
}


void EnemyLevelMediator::planGeneralPath(){

    initPathplanning();
    m_pathplanner->init();
    //Find a path from the starting cell in the grid. Column: 15, Row 31
    m_pathplanner->findPath(15,31);
    m_currentPath = m_pathplanner->getPath();
}

std::vector<Ogre::Vector3> EnemyLevelMediator::planSinglePath(int col, int row){
    initPathplanning();
    m_pathplanner->init();
    m_pathplanner->findPath(col,row);
    std::vector<Ogre::Vector3> singlePath = m_pathplanner->getPath();
    //Convert the path to coordinates
    return singlePath;
}

//TODO this isn't called currently
void EnemyLevelMediator::updateEnemyPaths(std::vector<Ogre::Vector3> path){
    //get enemies

    std::cout << "Updating paths" << std::endl;

    std::vector<Entity*> enemies = m_entity_manager->GetAllEnemies();

    std::cout<< "Number of enemies: " << m_entity_manager->countEnemies() << std::endl;
    for(auto it = enemies.begin(); it != enemies.end(); it++){
        std::cout << "Enemy ID: "<< (*it)->GetId() << std::endl;
        Enemy* pEnemy = (Enemy *)(*it);
        int lastIndex = pEnemy->GetCurrentPathIndex();

        pEnemy->PrintPath();
        //Pop the first few coordinates in the path until we arrive at the coordinate just before the collision happened
        std::vector<Ogre::Vector3> tmpPath = m_currentPath;

        std::reverse(tmpPath.begin(), tmpPath.end());
        for (int i = 0; i<lastIndex; i++){
            tmpPath.pop_back();
        }

        pEnemy->SetPath(&tmpPath);

        std::cout << "New Path of enemy: " << std::endl;
        for(auto it = tmpPath.begin(); it != tmpPath.end(); it++){
            std::cout << "Path after collision, X: "<< (*it).x << " Z: " << (*it).z << std::endl;
        }
    }
}


std::vector<Ogre::Vector3> EnemyLevelMediator::InitialEnemyPath(){

    std::vector<Ogre::Vector3> initialPath;
    Ogre::Vector3 start = Ogre::Vector3(1031.25, 0, -31.25);
    Ogre::Vector3 goal = Ogre::Vector3(-968.75, 0, -31.25);
    Ogre::Real diff = start.x - goal.x;

    while (diff > 0){

        Ogre::Real tmpValue = start.x;
        tmpValue -= Ogre::Real(62.5);
        start.x = tmpValue;
        initialPath.push_back(start);
        std::cout << "added value" << start.x << std::endl;
        diff-= 62.5;
    }

    std::reverse(initialPath.begin(), initialPath.end());
    return initialPath;
}

std::vector<Ogre::Vector3> EnemyLevelMediator::getCurrentPath(){
    return m_currentPath;
}

void EnemyLevelMediator::setCurrentPath(std::vector<Ogre::Vector3> path){
    std::cout<< "Setting Current Path" << std::endl;
    m_currentPath = path;
}

//For Debugging only
void EnemyLevelMediator::printGrid(){

    for(int col=31; col >= 0; col--){
        std::cout << std::endl;
        for(int row=0; row < 32; row++){
            if (m_GridArray[col][row] == 0){
                std::cout << "  ";
            }
            std::cout << m_GridArray[col][row];
        }
    }
    std::cout << std::endl;
}

//For Debugging only
void EnemyLevelMediator::printCurrentPath(){
    for(auto it = m_currentPath.begin(); it != m_currentPath.end(); it++){
        std::cout << "Part of path: "<< (*it).x << std::endl;
    }
}




